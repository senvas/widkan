/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate widkan;
use widkan::interface;
use widkan::widget;
use widkan::marp::image::AbstractImage;
use widkan::widget::*;
use widkan::marp::*;
use widkan::marp::command_buffer::*;
use widkan::marp::ash::vk;
use widkan::msw::winit;
use widkan::msw::WinitSurface;
use widkan::marp::swapchain::surface::Surface;
use widkan::config::Config;

use std::sync::Arc;
use std::u64;
#[allow(unused_imports)]
use std::time::Instant;

pub enum WindowingStyle{
    Maximised,
    Fullscreen,
    Hidden,
}
#[allow(dead_code)]
pub struct Window{
    window: winit::Window,
    events_loop: winit::EventsLoop,
    interface: Arc<interface::Interface>,

    instance: Arc<instance::Instance>,
    surface: Arc<dyn swapchain::surface::Surface>,
    device: Arc<device::Device>,
    queue: Arc<device::Queue>,
    swapchain: Arc<swapchain::Swapchain>,
    command_pool: Arc<command_buffer::CommandBufferPool>,
    command_buffer: Arc<command_buffer::CommandBuffer>,

    present_complete: Arc<sync::Semaphore>,
    render_complete: Arc<sync::Semaphore>,
    
    last_cursor_pos: (f64, f64),
    last_events: Vec<events::Event>,

    last_update_state: UpdateRetState,
}

impl Window{
    ///Creates the application window. You can provide a window build to specify the styling etc, or just use the default ones.
    pub fn new(
        window_builder: Option<winit::WindowBuilder>,
        config: &str,
    ) -> Result<Self, ()>{
        //Create an window and an instance
        let mut app_info = miscellaneous::AppInfo::default("MyApp".to_string());
        app_info.set_api_version(miscellaneous::Version::new(1,1,0));

        //Load the config and check if we should use wayland.
        let current_config = widkan::config::Config::load(config);
        
        //Currently disabling wayland since we wanna use renderdoc.
        let mut extensions = miscellaneous::InstanceExtensions::presentable();
        extensions.wayland_surface = current_config.support_wayland;
        extensions.wayland_surface = true;
        
        let layer = miscellaneous::InstanceLayer::debug_layers();

        //Main events loop and window
        let eventsloop = winit::EventsLoop::new();
        let window = if let Some(wb) = window_builder{
            wb.build(&eventsloop).expect("Failed to create window from WindowBuilder!")
        }else{
            winit::Window::new(&eventsloop).expect("Failed to create window")
        };

        let window_extent = window.get_inner_size().expect("Could not get windows inner size!");
        let swapchain_extent = vk::Extent2D::builder()
            .width(window_extent.width as u32)
            .height(window_extent.height as u32)
            .build();

        
        let last_cursor_pos = (0.0,0.0);
        //window.set_cursor_position((0.0,0.0).into()).expect("Failed to set cursor position");
        
        //The vulkan instance we'll be using
        let instance = match instance::Instance::new(
            Some(app_info),
            Some(extensions),
            Some(layer),
            None
        ){
            Ok(inst) => inst,
            Err(er) => {
                println!("failed to create instance: {}\ndoes your gfx card and driver support vulkan?", er);
                return Err(());
            },
        };

        //Create a surface for the window
        let surface: Arc<dyn Surface + Send + Sync> = WinitSurface::new(
            instance.clone(),
            &window,
            &eventsloop
        ).expect("Failed to create window");


        //Create a physical device 
        let p_devices = device::physical_device::PhysicalDevice::find_physical_device(instance.clone())
            .expect("failed to find physical devices");
        
        //find the physical device that supports presentation on out surface.
        let (p_device, index) = p_devices.into_iter().filter_map(|pdev|{
            match pdev.find_present_queue_family(&surface){
                Ok(queue_idx) => {
                    println!("widkan-window: DEBUG: Found suitable physical device");
                    Some((pdev, queue_idx))
                },
                Err(er) => {
                    println!("widkan-window: WARNING: Could not find presentable surface for physical device: {:?}", er);
                    None
                }
            }
        }).nth(0).expect("failed to find suitable device for presenting!");



        //Create one queue for the 
        let queue_create_info = vec![(*p_device.get_queue_family_by_index(index).expect("Failed to get graphics queue family"), 1.0 as f32)];
        let needed_device_ext = vec![
            miscellaneous::DeviceExtension::new("VK_KHR_swapchain".to_string(), 1),
        ];
        let (device, mut queues) = device::device::Device::new(
            instance.clone(),
            p_device.clone(),
            queue_create_info,
            Some(&needed_device_ext),
            Some(*p_device.get_features())
        ).expect("failed to find Device and its queues");
        println!("widkan-window: DEBUG: found {}, present queues", queues.len());
        let queue = queues.pop().expect("Could not get our present queue!");


        //Find a depth and color format that us unorm, otherwise the colors might look off
        let swapchain_format = surface
            .get_supported_formats(device.get_physical_device())
            .expect("Failed to get surface formats").into_iter().fold(None, |b, f|{
		println!("Found format: {:?}", f.format);
                if let None = b{
		    if f.format == vk::Format::B8G8R8A8_UNORM{
			Some(f)
		    }else{
			None
		    }
                }else{
                    b
                }
            }).expect("Could not find b8g8r8a8-unorm format");

        for i in surface.get_supported_formats(device.get_physical_device()).expect("No formats!"){
            println!("Format: \tforamt: {:#?}, cs: {:#?}", i.format, i.color_space);
        }
        
        //Create swapchain
        let swapchain = swapchain::Swapchain::new(
            device.clone(),
            surface.clone(),
            swapchain_extent,
            Some(swapchain_format),
            Some(2),
            Some(ash::vk::PresentModeKHR::IMMEDIATE),
            Some(image::ImageUsage{
                color_attachment: true,
                transfer_dst: true,
                .. Default::default()
            })
        ).expect("Failed to create swapchain!");
        let extent = swapchain.get_extent();
        let (ex,ey) = (extent.width as f64, extent.height as f64);
        
        let command_pool = CommandBufferPool::new(
            device.clone(),
            queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        ).expect("Failed to create command pool!");

        let command_buffer = command_pool.alloc(
            1,
            false
        ).expect("Failed to allocate main command buffer!").pop().expect("Did not get 1 command buffer");
        
        Ok(Window{
            window,
            events_loop: eventsloop,
            interface: widkan::interface::Interface::new(
                device.clone(),
                queue.clone(),
                (ex,ey),
                config,
                swapchain_format.format
            ),

            instance,
            surface,
            device: device.clone(),
            queue,
            swapchain,
            command_pool,
            command_buffer,

            
            present_complete: sync::Semaphore::new(device.clone()).expect("Failed to create present_complete semaphore"),
            render_complete: sync::Semaphore::new(device.clone()).expect("Failed to create render_complete semaphore"),

            last_cursor_pos,
            last_events: Vec::new(),
            last_update_state: Default::default(),
        })
    }

    ///Sets the widget tree which will be rendered at the next update
    pub fn set_widgets(&self, widgets: Arc<dyn Widget + Send + Sync>){
        self.interface.set_root(widgets);
    }

    ///Handles the widgets return states
    fn handle_ret_state(&mut self, upd: UpdateRetState){
        //Windows cursor state
        if self.last_update_state.new_cursor != upd.new_cursor{
            self.window.set_cursor(upd.new_cursor);    
        }

        //Override state info
        self.last_update_state = upd;
    }
    
    ///Re-renders the interface and presents it when finished, return `false` if the program should end
    pub fn update(&mut self){
        //First analyze the events for an resize
        let mut winit_events = Vec::new();
        self.events_loop.poll_events(|ev|{
            winit_events.push(ev);
        });
        
        //Convert the events into native events
        let (events, new_cursor_pos) = widget::events::to_events(
            self.last_cursor_pos,
            winit_events,
            &self.window
        );
        //Update the cursor position
        self.last_cursor_pos = new_cursor_pos;

        //Check if have a resize event, in which case we have to regenerate the window
        // However, since multiple can occur we just take the last one into account
        let mut most_recent_area = self.interface.area();
        for ev in events.iter(){
            if let events::EventType::Resize(new_area) = ev.event{
                most_recent_area = new_area;
            }
        }

        let debug_start_time = Instant::now();

        if most_recent_area != self.interface.area(){
            //A resize with new dims occurred, therefore we have to resize our window.
            self.resize(most_recent_area);
        }

        
        
        //Now reset the command buffer, let the toolkit add render commands and finally present to swapchain.
        self.command_buffer.reset().expect("Failed to reset command buffer");
        self.command_buffer.begin_recording(
            true, //One time submit yes
            false, //No continue
            false, //No simultaneouse
            None
        ).expect("Failed to begin command buffer!");
        
        //Now update the interface with the new events.
        let ret_state = self.interface.update(&events, self.queue.clone());
        //immediately render the interface. A performance optimization could track if there was any change, and if not just present the old image.
        // therefore the gpu would not work when the interface did not change.
        self.interface.render(self.command_buffer.clone());
        
        let _time_till_exec_ms = debug_start_time.elapsed().as_millis();
        let _time_till_exec_ns = debug_start_time.elapsed().as_nanos();

        self.handle_ret_state(ret_state);

        //Check which image we are going to use
        let submit_index = match self.swapchain.acquire_next_image(
            u64::MAX,
            self.present_complete.clone(), //Signals when the image at index is actually available.
        ){
            Ok(idx) => idx,
            Err(_) => {
                println!("widkan-window: WARNING: could not get image index... dropping frame");
                return;
            }
        };
        
        //Transfer the swapchain image to transfer dst, copy then transfer back
        self.command_buffer.cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.swapchain.get_images()[submit_index as usize].clone().new_image_barrier(
                    Some(ash::vk::ImageLayout::PRESENT_SRC_KHR),
                    Some(ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
                )]
            ).expect("Failed to add pipeline barrier while copying");
        
        //After the interface updated the internal image, we blit it to the swapchain image
        self.command_buffer.cmd_copy_image(
            self.interface.get_image(),
            vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            self.swapchain.get_images()[submit_index as usize].clone(), //Since we wait for the image anyways, we can do this
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vec![
                vk::ImageCopy::builder()
                    .src_subresource(vk::ImageSubresourceLayers{
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    }).src_offset(
                        vk::Offset3D{x:0, y:0, z:0}
                    ).dst_subresource(vk::ImageSubresourceLayers{
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    }).dst_offset(
                        vk::Offset3D{x:0, y:0, z:0}    
                    ).extent(
                        self.interface.get_image().extent_3d()
                    ).build()
            ]
        ).expect("Failed to copy final image to swapchain image!");

        self.command_buffer.cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::TOP_OF_PIPE,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.swapchain.get_images()[submit_index as usize].clone().new_image_barrier(
                    Some(ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    Some(ash::vk::ImageLayout::PRESENT_SRC_KHR),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
                )]
            ).expect("Failed to add pipeline barrier while copying");
        
        
        //Now try to present the cb
        self.command_buffer.end_recording().expect("Failed to end command buffer!");

        let render_compleat_fence = self.queue.queue_submit(
            vec![
                device::queue::SubmitInfo::new(
                    vec![(self.present_complete.clone(), vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)],
                    vec![self.command_buffer.clone()],
                    vec![self.render_complete.clone()]
                )
            ]
        ).expect("Failed to submit work to graphics queue");
        //Wait for the Graphics queue to end presenting.
        render_compleat_fence.wait(u64::MAX).expect("Failed to wait for GPU");

        match self.swapchain.queue_present(
            self.queue.clone(),
            vec![self.render_complete.clone()],
            submit_index
        ){
            Ok(_) => {},
            Err(er) => {
                println!("Could not present new frame: {}\ndropping frame!", er);
            }
        }
        //println!("===\n\n\n===");
        self.last_events = events;
        //println!("Content update time: {}ms / {}ns", time_till_exec_ms, time_till_exec_ns);
        //println!("RenderTime: {}ms / {}ns",
        //         debug_start_time.elapsed().as_millis() - time_till_exec_ms,
        //         debug_start_time.elapsed().as_nanos() - time_till_exec_ns);
    }

    ///Returns the events that occurred on the last update.
    pub fn events(&self) -> &Vec<events::Event>{
        &self.last_events
    }

    fn resize(&mut self, new_area: Area){
        //Update the swapchain
        //let window_extent = self.window.get_inner_size().expect("Could not get windows inner size!");
        let swapchain_extent = vk::Extent2D::builder()
            .width(new_area.extent.0 as u32)
            .height(new_area.extent.1 as u32)
            .build();

        self.swapchain.recreate(swapchain_extent);
        let submit_fence = self.swapchain.images_to_present_layout(self.queue.clone());
        submit_fence.wait(u64::MAX).expect("Failed to wait for layout transition");
    }

    ///Returns the Interface instance that is used for rendering.
    pub fn interface(&self) -> Arc<interface::Interface>{
        self.interface.clone()
    }

    ///Sends out a copy of this config
    pub fn get_config(&self) -> Config{
        self.interface.get_config()
    }

    ///Sets the config within this windows interface
    pub fn set_config(&self, new_config: Config){
        self.interface.set_config(new_config);
    }
}
