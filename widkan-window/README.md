# Widkan-Window
## Description
Widkan-Window is a helper crate which takes care of window creation and 
event processing when using widkan as an pure widget-toolkit.
This means you are not using vulkan/marp to do any work and use widkan as you ui-toolkit.

## Usage
The crate can be called to create a interface for some program. You can specify the widget tree you want to render, and you can choose at which 
point the interface is updated. 

## Used crates
- widkan
- widkan::marp: vulkan object creation
- widkan::marp::winit for window and surface creation
