echo "Compiling shader"
echo "==="
echo ""

#Cube with textures example
echo "Compile Rectangle shader ..."
glslangValidator -V rectangle/rect.frag 
glslangValidator -V rectangle/rect.vert
mv frag.spv rectangle/
mv vert.spv rectangle/

echo "Compile Text shader ..."
glslangValidator -V text/text.frag 
glslangValidator -V text/text.vert
mv frag.spv text/
mv vert.spv text/

echo "Compile Line shader ..."
glslangValidator -V line/line.frag 
glslangValidator -V line/line.vert
mv frag.spv line/
mv vert.spv line/

echo "Compile Icon shader ..."
glslangValidator -V icon/icon.frag 
glslangValidator -V icon/icon.vert
mv frag.spv icon/
mv vert.spv icon/

echo ""
echo "==="
echo "Finished compiling shaders"
