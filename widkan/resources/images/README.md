# Content of the images folder
- "IconMap.png" contains a tile map with all standart icons that are accessable from the `Icon` primitive via the `Ico` enum
- "IconMap.xcf" contains the gimp-save of the icon tile map. It also has an raster in the second layer. If you add new icons, please add
them to a new layer so that it is easy to manage.

The SVG image are the source files to the small versions used in IconMap.xfc
