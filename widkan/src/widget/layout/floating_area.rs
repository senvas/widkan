/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use crate::rendering::primitive::*;
use std::sync::{Arc, RwLock};

///Describes the positioning strategy of the child widget.
pub enum Positioning{
    ///If the child area should be positioned based on alignment rules
    Alignment(Alignment),
    ///If the child widget should be fix positioned. The position is calculated based on the top left of the
    /// Host. So a position of (2.0,2.0) will place it two pixels on x/y down right from the top left of the host.
    FixPosition((f64, f64))
}

///Places a child widget into a given area which is placed at a given position.
///This can be used to place an always same sized widget within an maybe bigger area.
/// Like buttons.
pub struct FloatingArea{
    area: RwLock<Area>,
    child: Arc<dyn Widget + Send + Sync>,
    child_max_area: Area,
    child_pos: Positioning,
}

impl FloatingArea{
    ///Note on `childs_area`, in this case the extent of the area is most important, since the pos is changed depending on the hosts
    /// extent and location.
    pub fn new(
        child: Arc<dyn Widget + Send + Sync>,
        childs_area: Area,
        child_position: Positioning
    ) -> Arc<Self>{
        Arc::new(
            FloatingArea{
                area: RwLock::new(Area::empty()),
                child,
                child_max_area: childs_area,
                child_pos: child_position
            }
        )
    }
    ///Returns the host area, not the child area, for that use `get_child_area()`
    pub fn area(&self) -> Area{
        *self.area.read().expect("Could not read floating area")
    }

    pub fn get_child_area(&self) -> Area{
        //Depending on the chosen positioning, calculate the sub area.
        match self.child_pos{
            Positioning::Alignment(ref al) => {
                al.to_aligned_area(self.area(), self.child_max_area)
            },
            Positioning::FixPosition(pos) => {
                //Calc the new pos and have a look that we are not extenting the bounds
                let sarea = self.area();
                let mut new_area = Area{
                    pos: (sarea.pos.0 + pos.0, sarea.pos.1 + pos.1),
                    extent: (self.child_max_area.extent.0, self.child_max_area.extent.1)
                };

                new_area.clamp_within(&sarea);

                new_area
            }
        }
    }
}


impl Widget for FloatingArea{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        let mut child_calls = self.child.render(renderer, level + 1);
        //Render a panel for us
        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            self.area(),
            level,
            DrawType::Theme(Theme::Pattern),
        ));

        child_calls
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        //We don't process any updates, therefore just update the child
        *self.area.write().expect("Failed to update floating host area") = new_area;
        self.child.update(self.get_child_area(), events, renderer)
    }
}
