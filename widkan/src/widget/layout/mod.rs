/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/// Places a sub-widget at a location within this widgets area with a given max size.
pub mod floating_area;
pub use floating_area::*;

///Arranges two sub-widgets either horizontally or vertically.
pub mod layout_box;
pub use layout_box::*;


///A bar which can fit n items each with the same size.
pub mod bar;
pub use bar::*;


///Simulates a bigger area which can be controlled through
/// scrolling.
pub mod viewport;
pub use viewport::*;

///A Simple horizontal or vertical list of widget. It has no
/// scroll bar. Instead the children all get the same amount of space
pub mod static_list;
pub use static_list::*;
