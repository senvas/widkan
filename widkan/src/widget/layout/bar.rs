/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use crate::config::Config;
use std::sync::{Arc, RwLock};


///Can fit several items, Each item will have the same size. Unused space
/// is drawn with the standard background.
pub struct Bar{
    //The horizontal split between the buttons and the content
    root: Arc<layout::LayoutBox>,
}


impl Bar{
    ///Takes a set of sub widgets as well as their titles
    pub fn new(
        config: &Config,
        items: Vec<(String, Arc<dyn Widget + Send + Sync>)>
    ) -> Arc<Self>{
        //Create a vertical split. The top is the row of buttons assembled in a list each with the string given as the title.
        // The buttons created activate the supplied widget when they are pressed as the "current child"

        //Setup current child which will be overwritten.
        let root = layout::LayoutBox::new(
            config,
            None,
            None,
            layout::SplitType::VerticalFixed(layout::Offset::PixelLeft(config.default_button_extent.1))
        );
        
        let buttons: Vec<_> = items.into_iter().map(|(title, widget)|{
            let inner_root = root.clone();
            let button: Arc<dyn Widget + Send + Sync> = io::Button::new(
                config,
                io::ButtonContent::Text(title.to_owned()),
                io::Action::captured_action(move |d| inner_root.set_second(d.clone()), widget)
            );

            button
        }).collect();

        let list = layout::StaticList::new(&config, buttons, layout::SLDirection::Horizontal);

        //Set the list view as the first child
        root.set_first(
            list
        );

        Arc::new(
            Bar{
                root
            }   
        )
    }
}

impl Widget for Bar{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        self.root.render(renderer, level)
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        self.root.update(new_area, events, renderer)
    }
}
