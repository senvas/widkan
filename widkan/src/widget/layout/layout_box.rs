/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::widget::io::*;
use crate::widget::*;
use std::sync::{Arc, RwLock};
use msw::winit::ElementState;
use msw::winit::MouseButton;
use msw::winit;

///Defines in which measure, from which border the offset is calculated.
/// If used vertically, Left == Top, Right == Bottom.
#[derive(PartialEq, Clone, Copy)]
pub enum Offset{
    PixelLeft(f64),
    PixelRight(f64),
    PercentLeft(f64),
    PercentRight(f64)
}

impl Offset{
    fn to_percentage(&self, axis_extent: f64) -> f64{
        match self{
            Offset::PixelLeft(x) => {
                (x / axis_extent).min(1.0).max(0.0)
            },
            Offset::PixelRight(x) => {
                1.0 - (x / axis_extent).min(1.0).max(0.0)
            }
            Offset::PercentLeft(x) => {
                x.min(1.0).max(0.0)
            },
            Offset::PercentRight(x) => {
                1.0 - x.min(1.0).max(0.0)
            }
        }
    }

    fn from_percentage(&mut self, mut percent: f64, axis_extent: f64){

        percent = percent.min(1.0).max(0.0);
        match self{
            Offset::PixelLeft(ref mut x) => {
                *x = percent * axis_extent;
            },
            Offset::PixelRight(ref mut x) => {
                *x = axis_extent - (percent * axis_extent);
            }
            Offset::PercentLeft(ref mut x) => {
                *x = percent.min(1.0).max(0.0);
            },
            Offset::PercentRight(ref mut x) => {
                *x = 1.0 - percent.min(1.0).max(0.0);
            }
        }
    }
}

#[derive(PartialEq, Clone, Copy)]
pub enum SplitType {
    ///The border between left and right is moveable. It is initial set at the given percentage
    HorizontalMoveable(Offset),
    ///The border is not moveable. It is set at the given percentage
    HorizontalFixed(Offset),

    VerticalMoveable(Offset),
    VerticalFixed(Offset),
}

impl SplitType{
    fn set_percentage(&mut self, perc: f64, host_area: Area){
        match self{
            SplitType::HorizontalFixed(ref mut x) | SplitType::HorizontalMoveable(ref mut x) => {
                x.from_percentage(perc, host_area.extent.0);
            },
            SplitType::VerticalFixed(ref mut x) | SplitType::VerticalMoveable(ref mut x) => {
                x.from_percentage(perc, host_area.extent.1);
            }
        }
    }
    
    fn get_percentage(&self, host_area: Area) -> f64{
        match self{
            SplitType::HorizontalFixed(x) | SplitType::HorizontalMoveable(x) =>{
                x.to_percentage(host_area.extent.0)
            },
            SplitType::VerticalFixed(x) | SplitType::VerticalMoveable(x) =>{
                x.to_percentage(host_area.extent.1)
            }
        }
    }

    fn is_vertical(&self) -> bool{
        match self{
            SplitType::HorizontalFixed(_) | SplitType::HorizontalMoveable(_) => false,
            _ => true
        }
    }

    fn is_moveable(&self) -> bool{
        match self{
            SplitType::HorizontalMoveable(_) | SplitType::VerticalMoveable(_) => true,
            _ => false
        }
    }
}

///Takes two children and aligns them
pub struct LayoutBox {
    first: RwLock<Arc<dyn Widget + Send + Sync>>,
    second: RwLock<Arc<dyn Widget + Send + Sync>>,
    area: RwLock<Area>,
    //If some, then the u32 is the border width
    split_size: u32,
    trigger_size: u32,
    //Percentage of the widget at which left and right are split
    split_type: RwLock<SplitType>,
    //Tracks if the cursor was pressed on the split area, but didn't release yet
    mouse_pressed: RwLock<bool>,
}

impl LayoutBox {
    pub fn new(
        config: &Config,
        first: Option<Arc<dyn Widget + Send + Sync>>,
        second: Option<Arc<dyn Widget + Send + Sync>>,
        split_type: SplitType,
    ) -> Arc<Self> {
        Arc::new(LayoutBox {
            first: RwLock::new(if let Some(r) = first {
                r
            } else {
                panel::Panel::new()
            }),
            second: RwLock::new(if let Some(l) = second {
                l
            } else {
                panel::Panel::new()
            }),
            split_type: RwLock::new(split_type),
            area: RwLock::new(Area {
                pos: (0.0, 0.0),
                extent: (0.0, 0.0),
            }),
            split_size: config.border_width,
            trigger_size: config.resizing_trigger,
            mouse_pressed: RwLock::new(false),
        })
    }

    pub fn set_first(&self, first: Arc<dyn Widget + Send + Sync>) {
        *self.first.write().expect("Could not set LayoutBox child") = first;
    }

    pub fn set_second(&self, second: Arc<dyn Widget + Send + Sync>) {
        *self.second.write().expect("Could not set LayoutBox child") = second;
    }

    pub fn split(&self) -> SplitType{
        *self.split_type.read().expect("Failed to get inner split type!")
    }

    ///Returns the area currently occupied by this widget
    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not get inner areas!")
    }

    ///Returns the Left/Right or Top/Bottom areas of its children.
    fn get_areas(&self) -> (Area, Area) {
        let this_area: Area = self.get_area();
        let split = self.split().get_percentage(this_area);
        //Depending on the layout type, calc pos and extent
        if self.split().is_vertical() {
            let first = Area {
                    pos: this_area.pos,
                    extent: (
                        this_area.extent.0,
                        this_area.extent.1 * split - (self.split_size as f64 / 2.0),
                    ),
                };
                let second = Area {
                    pos: (
                        this_area.pos.0,
                        this_area.pos.1
                            + this_area.extent.1 * split
                            + (self.split_size as f64 / 2.0),
                    ),
                    extent: (
                        this_area.extent.0,
                        this_area.extent.1 * (1.0 - split) - (self.split_size as f64 / 2.0),
                    ),
                };
            (first, second)
        }else{
            let first = Area {
                pos: this_area.pos,
                extent: (
                    this_area.extent.0 * split - (self.split_size as f64 / 2.0),
                    this_area.extent.1,
                ),
            };
            let second = Area {
                pos: (
                    this_area.pos.0
                        + this_area.extent.0 * split
                        + (self.split_size as f64 / 2.0),
                    this_area.pos.1,
                ),
                extent: (
                    this_area.extent.0 * (1.0 - split) - (self.split_size as f64 / 2.0),
                    this_area.extent.1,
                ),
            };
            (first, second)
        }
        
    }
}

impl Widget for LayoutBox {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{        
        let mut child_calls = self.first
            .read()
            .expect("LayoutBox, could not read first child")
            .render(renderer, level + 2);
        
        child_calls.append(
            &mut self.second
                .read()
                .expect("LayoutBox, could not read second child")
                .render(renderer, level + 2));
        
        //Render the possible frame between the sub areas as well as an default plane for each area
        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            *self.area.read().expect("Could not read rect area"),
            level,
            DrawType::Theme(Theme::EdgePattern),
        ));
        let (left, right) = self.get_areas();
        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            left,
            level + 1,
            DrawType::Theme(Theme::Pattern),
        ));

        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            right,
            level + 1,
            DrawType::Theme(Theme::Pattern),
        ));

        child_calls
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {
        //If things can be resized, we have to check clicks and move motions
        let mut mouse_pressed = *self
            .mouse_pressed
            .read()
            .expect("Could not get mouse pressed status");
        // init cursor state
        let mut cursor = if mouse_pressed {
            if self.split().is_vertical() {
                winit::MouseCursor::RowResize
            } else {
                winit::MouseCursor::ColResize
            }
        } else {
            winit::MouseCursor::Default
        };

        if self.split().is_moveable(){
            //Check if we have to set the cursor state

            //Find the area we have to consider when testing mouse events.
            let split = self.split().get_percentage(new_area);
            let split_area = if self.split().is_vertical() {
                Area {
                    pos: (
                        new_area.pos.0,
                        new_area.pos.1 + (new_area.extent.1 * split)
                            - (self.trigger_size as f64 / 2.0),
                    ),
                    extent: (new_area.extent.0, self.trigger_size as f64),
                }
            }else{
                Area {
                    pos: (
                        new_area.pos.0 + (new_area.extent.0 * split)
                            - (self.trigger_size as f64 / 2.0),
                        new_area.pos.1,
                    ),
                    extent: (self.trigger_size as f64, new_area.extent.1),
                }
            };

            //println!("Split area: {:?}", split_area);

            for ev in events {
                //First check if the event should be considered for a move
                match ev.event {
                    EventType::MouseClick {
                        state,
                        button,
                        modifiers: _,
                    } => {
                        //Check if the right key started klicking, is so, from now till klick has ended every motion must be checked.
                        if button == MouseButton::Left {
                            if state == ElementState::Pressed {
                                //Only add the mouse press, if it was on our location
                                if split_area.is_in(ev.location) {
                                    mouse_pressed = true;
                                    if self.split().is_vertical() {
                                        cursor = winit::MouseCursor::RowResize;
                                    } else {
                                        cursor = winit::MouseCursor::ColResize;
                                    }
                                }
                            } else {
                                mouse_pressed = false;
                                cursor = winit::MouseCursor::Default;
                            }
                        }
                    }
                    EventType::CursorMoved {
                        position: _,
                        delta,
                        modifiers: _,
                    } => {
                        if mouse_pressed {
                            //Check how much of an delta we have on the "right" axis
                            if self.split().is_vertical() {
                                //Check the movement on y
                                let delta_percentage = delta.1 / new_area.extent.1;
                                let new_split = self.split().get_percentage(new_area) + delta_percentage;
                                
                                //Now override the split field
                                self.split_type.write().expect("Could not override new split!").set_percentage(new_split, new_area)
                            }else{
                                //Check the movement on x
                                let delta_percentage = delta.0 / new_area.extent.0;
                                let new_split = self.split().get_percentage(new_area) + delta_percentage;
                                
                                //Now override the split field
                                self.split_type.write().expect("Could not override new split!").set_percentage(new_split, new_area)
                            }
                        }
                    }
                    _ => {} //Other events are not used
                }
            }
        }

        //Update our own area
        *self.area.write().expect("Could not write panel area") = new_area;
        
        //Update mouse status
        *self
            .mouse_pressed
            .write()
            .expect("Could not write mouse status") = mouse_pressed;
        //Now update children
        let (first_new, second_new) = self.get_areas();
        
        let new_state_one = self
            .first
            .read()
            .expect("Failed to read first child")
            .update(first_new, events, renderer);
        let new_state_two = self
            .second
            .read()
            .expect("Failed to read first child")
            .update(second_new, events, renderer);

        let this_state = UpdateRetState {
            new_cursor: cursor,
            ..Default::default()
        };

        //Create final state
        this_state.merge(new_state_one.merge(new_state_two))
    }
}

