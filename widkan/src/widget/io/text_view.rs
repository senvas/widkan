/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use crate::config::Config;
use crate::widget::helper::*;
use crate::widget::*;

use crate::rendering::primitive::*;
use std::sync::{Arc, RwLock};


///Contrary to the text edit and the label, this widget displays multiple lines of text.
/// It also places the text into an automatically growing Viewport, if one is needed.
pub struct TextView{
    area: RwLock<Area>,
    //Holds the viewport that can be used to render the text.
    viewport: Arc<layout::Viewport>,
    use_viewport: RwLock<bool>,
    
    text: Arc<TextBox>,
    border_width: u32,
    ///Holds the default height if not set by the parent.
    height: f64,
}

impl TextView{
    pub fn new(
        config: &Config,
        default_text: &str,
        text_size: Option<u32>,
        alignment: Option<Alignment>,
    ) -> Arc<Self>{

        let text_widget = TextBox::new(
            config,
            default_text.to_string(),
            if let Some(s) = text_size{
                s
            }else{
                config.text_size
            },
            if let Some(a) = alignment{
                a
            }else{
                Alignment::UpLeft
            },
            true,
            true,
            true
        );
        
        Arc::new(
            TextView{
                area: RwLock::new(Area::empty()),
                viewport: layout::Viewport::new(
                    config,
                    layout::ViewDimension::Vertical(0.0),
                    text_widget.clone()
                ),
                use_viewport: RwLock::new(false),
                text: text_widget,
                border_width: config.slim_border_width,
                height: config.default_single_line_height,
            }
        )
    }

    //Returns the current text in the edit
    pub fn get_text(&self) -> String{
        self.text.get_text()
    }

    pub fn set_text(&self, text: String){
        self.text.set_text(text);
    }

    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not read textbox area")
    }
    
    fn text_box_area(&self) -> Area{
        self.get_area().from_frame(self.border_width)
    } 
}

impl Widget for TextView{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        //Render the Frame and the drawing areas background

        //First render the text or viewport if needed on this level + 2
        
        let child_calls = if *self.use_viewport.read().expect("Could not read use_viewport flag"){
            self.viewport.render(renderer, level + 1)
        }else{
            self.text.render(renderer, level + 1)
        };
        
        //In this level render the outline of the text edit

        

        child_calls
    }
    
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        *self.area.write().expect("Failed to update TextEdit area") = new_area;        
        let text_height = self.text.get_text_height(renderer);

        //Since we use and old text height, this lags behind one frame... but shouldn't be visible...
        if text_height > self.get_area().extent.1{
            *self.use_viewport.write().expect("Could not update use viewport flag") = true;
            self.viewport.set_view_dimension(layout::ViewDimension::Vertical(text_height));
            self.viewport.update(new_area, events, renderer)
        }else{
            *self.use_viewport.write().expect("Could not update use viewport flag") = false;
            self.text.update(self.text_box_area(), events, renderer)
        }
    }
}

impl ListEntry for TextView{
    fn get_height(&self) -> f64{
        self.height
    }
}
