/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::widget::io::*;
use crate::widget::*;

use std::sync::{Arc, RwLock};

pub struct Frame {
    inner: RwLock<Arc<dyn Widget + Send + Sync>>,
    area: RwLock<Area>,
    border_width: u32,
}

impl Frame {
    pub fn new(config: &Config, child: Option<Arc<dyn Widget + Send + Sync>>) -> Arc<Self> {
        Arc::new(Frame {
            inner: if let Some(wid) = child {
                RwLock::new(wid)
            } else {
                //Else add some panel
                RwLock::new(Panel::new())
            },
            area: RwLock::new(Area {
                pos: (0.0, 0.0),
                extent: (0.0, 0.0),
            }),
            border_width: config.border_width,
        })
    }
}

impl Widget for Frame {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        //We draw a frame by just covering the whole area and adding the children on top
        let mut child_calls = self.inner
            .read()
            .expect("Failed to read inner")
            .render(renderer, level + 1);

        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            *self.area.read().expect("Could not read rect area"),
            level,
            DrawType::Theme(Theme::EdgePattern),
        ));

        child_calls
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {
        *self.area.write().expect("Could not write Frame area") = new_area;
        //We now calculate the new area for the sub frame based on the border width.
        let child_area = Area {
            pos: (
                (new_area.pos.0 + self.border_width as f64),
                (new_area.pos.1 + self.border_width as f64),
            ),
            extent: (
                (new_area.extent.0 - (2.0 * self.border_width as f64)),
                (new_area.extent.1 - (2.0 * self.border_width as f64)),
            ),
        };

        self.inner
            .read()
            .expect("Failed to read frames child")
            .update(child_area, events, renderer)
    }
}
