/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::widget::*;

use std::sync::{Arc, RwLock};
use msw::winit::ElementState;
use msw::winit::MouseButton;
use msw::winit::VirtualKeyCode;

use clipboard::ClipboardProvider;
use clipboard::ClipboardContext;

///Describes the selection state of this fields text
#[derive(Clone, Copy, Debug)]
pub enum Selection {
    NoSelection,
    Selection {
        //The start chars_index, as well as the percentage at which it was clicked
        start: (usize, f64),
        //The end chars index
        end: (usize, f64),
        //True if the mouse button was already lifted and the selection has finished
        has_ended: bool,
    },
    //If the selection is currently a cursor at a location in the buffer
    CursorAt(usize),
}

impl Selection {
    //Might switch start and end if start > end, changes start and end selection to cover only whole characters
    pub fn to_character_align_selection(&mut self) {
        if let Selection::Selection {
            ref mut start,
            ref mut end,
            has_ended: _,
        } = self
        {
            if start.0 == end.0 && start.1 == end.1 {
                let cursor_selection = if start.1 > 0.5 { start.0 + 1 } else { start.0 };
                *self = Selection::CursorAt(cursor_selection);
                return;
            }

            //Check if we need to swap start and end
            if start.0 > end.0 {
                let old_start = *start;
                let old_end = *end;
                *start = old_end;
                *end = old_start;
            } else if start.0 == end.0 && start.1 > end.1 {
                let old_start = *start;
                let old_end = *end;
                *start = old_end;
                *end = old_start;
            }

            //Check if we need to move and start/end selections
            if start.1 > 0.5 {
                start.0 += 1;
                start.1 = 0.0;
            }

            if end.1 < 0.5 {
                end.0 -= 1;
                end.1 = 1.0;
            }
        }
    }
}

enum MoveDirection {
    Left,
    Right,
    Up,
    Down,
}

///Can be use to controll the text boxes text based on the current selection.
enum InsertPrimitves {
    String(String),
    ///If true it will delete the char in the front, else the one the cursor is on. When in a selection
    ///it doesn't matter
    Delete(bool),
    ///Copy the current selection to clipboard, if true, the selection will then be removed (like ctrl+x)
    CopyToClipboard(bool),
    Nothing,
}

///A text box with a certain size displaying single or multi-line Strings.
/// While this is a widget it should be used as a helper widget in general.
/// #Warning
/// Only the letters will be rendered, the parent widget has to take care of the background!
pub struct TextBox {
    ///Holds the current text to be displayed in a set of chars together with their width.
    raw_text: RwLock<String>,
    size: u32,
    character_white_list: String,
    area: RwLock<Area>,
    alignment: Alignment,
    multi_line: bool,
    selectable: bool,
    editable: bool,

    cursor_width: f32,

    //If the text was selected
    selection: RwLock<Selection>,
}

impl TextBox {
    pub fn new(
        config: &Config,
        text: String,
        size: u32,
        alignment: Alignment,
        multi_line: bool,
        selectable: bool,
        editable: bool,
    ) -> Arc<Self> {
        Arc::new(TextBox {
            raw_text: RwLock::new(text),
            size,
            character_white_list: config.font_characters.clone(),
            area: RwLock::new(Area::empty()),
            alignment,
            multi_line,
            selectable,
            editable,
            cursor_width: config.text_cursor_width,
            selection: RwLock::new(Selection::NoSelection),
        })
    }

    pub fn set_text(&self, text: String) {
        *self
            .raw_text
            .write()
            .expect("Could not update text_boxs raw text") = text;
    }

    pub fn get_text(&self) -> String {
        self.raw_text.read().expect("Could not lock text").clone()
    }

    pub fn get_area(&self) -> Area {
        *self.area.read().expect("Failed to read labels area")
    }

    ///Returns the extent in pixels this text need vertically, to fit into the current, horizontal space
    pub fn get_text_height(&self, renderer: &Renderer) -> f64{
        let line_area = self.get_text_area(renderer);
        let height: f64 = line_area.into_iter().fold(0.0, |height, (_chars, line_area)| height + line_area.extent.1);
        height
    } 
    
    pub fn get_selection(&self) -> Selection {
        *self.selection.read().expect("Could not get selection")
    }
    ///Returns a set of text lines where each line is build form a chars and their areas in the form of:
    /// `Lines<CharsOnLine<Char, CharArea>, LineArea>`
    pub fn get_text_area(&self, renderer: &Renderer) -> Vec<(Vec<(char, Area)>, Area)> {
        //First split our main text in a way that it fits into the drawing area if we are multiline
        if self.multi_line {
            //First split the string into lines, then align them
            let fitted_strings =
                renderer
                    .text_primitive()
                    .fit_string(self.get_text(), self.get_area(), self.size);
            let current_area = self.get_area();
            let line_height = renderer.text_primitive().line_height(self.size);
            let mut string_area_pairs: Vec<(_, _)> = fitted_strings
                .into_iter()
                .map(|st| {
                    (
                        st.clone(),
                        Area {
                            pos: current_area.pos,
                            extent: (
                                renderer.text_primitive().string_length(&st, self.size) as f64,
                                line_height as f64,
                            ),
                        },
                    )
                })
                .collect();

            let max_extent_x = string_area_pairs
                .iter()
                .fold(0.0 as f64, |max_ex, (_str, area)| max_ex.max(area.extent.0));

            //Align the lines each, then recalculate the y pos to fit them above each other
            let mut text_area = Area {
                pos: current_area.pos,
                extent: (
                    max_extent_x,
                    string_area_pairs.len() as f64 * line_height as f64,
                ),
            };

            //We align the text area to know where to start with our y values for the final area pos arguments. for the x values
            // we align each line separately.
            text_area = self.alignment.to_aligned_area(current_area, text_area);
            //Update the y pos

            for (idx, (_c, ref mut area)) in string_area_pairs.iter_mut().enumerate() {
                *area = self.alignment.to_aligned_area(current_area, *area);
                area.pos.1 = text_area.pos.1 + (idx as f64 * line_height as f64);
            }

            //Now we build the final nested vec by finding the area per char
            let mut fin_vec = Vec::with_capacity(string_area_pairs.len());
            for (line_text, line_area) in string_area_pairs.into_iter() {
                let mut char_vec = Vec::with_capacity(line_text.chars().count());
                let mut x_add = 0.0;
                for c in line_text.chars() {
                    let char_width = renderer.text_primitive().char_length(c, self.size);
                    char_vec.push((
                        c,
                        Area {
                            pos: (line_area.pos.0 + x_add, line_area.pos.1),
                            extent: (char_width as f64, line_height as f64),
                        },
                    ));

                    x_add += char_width as f64;
                }

                fin_vec.push((char_vec, line_area));
            }

            fin_vec
        } else {
            let text_area = Area {
                pos: self.get_area().pos,
                extent: (
                    renderer
                        .text_primitive()
                        .string_length(&self.get_text(), self.size) as f64,
                    renderer.text_primitive().line_height(self.size) as f64,
                ),
            };

            //Now align the area
            let aligned_area = self.alignment.to_aligned_area(self.get_area(), text_area);
            let line_height = renderer.text_primitive().line_height(self.size);
            let mut x_offset = 0.0;
            let mut line_vec = Vec::with_capacity(self.get_text().chars().count());
            for c in self.get_text().chars() {
                let char_length = renderer.text_primitive().char_length(c, self.size);
                line_vec.push((
                    c,
                    Area {
                        pos: (aligned_area.pos.0 + x_offset, aligned_area.pos.1),
                        extent: (char_length as f64, line_height as f64),
                    },
                ));

                x_offset += char_length as f64;
            }

            vec![(line_vec, aligned_area)]
        }
    }

    ///Converts an index which points to some character in the string to an actual utf-8 index.
    /// The problem we have is that in the selection the index points to "whole" characters. so "ä" is the same
    /// length as "a" and "¼".
    /// However, in utf-8 a has the length 1 and the others have the length 2. Because of that the index has to be
    /// Calculated based on the current text and the index that is needed.
    pub fn selection_index_to_utf_index(&self, index: usize) -> usize {
        self.get_text()
            .chars()
            .enumerate()
            .fold(0, |current_len, (cidx, ch)| {
                if cidx < index {
                    current_len + ch.len_utf8()
                } else {
                    current_len
                }
            })
    }

    ///Returns the linear index at which something was selected. And the x_axis percentage at which it was clicked.
    fn index_for_position(
        &self,
        position: (f64, f64),
        lines: &Vec<(Vec<(char, Area)>, Area)>,
    ) -> (usize, f64) {
        //Since we are in between the lines, find the real index at which we are colliding.
        let mut current_idx = 0;
        for (_lidx, (line, line_area)) in lines.iter().enumerate() {
            //Check if the hit was somewhere in that line
            if line_area.is_in(position) {
                for (idx, (_ch, ch_area)) in line.iter().enumerate() {
                    if let Some((x_perc, _y_perc)) = ch_area.hit_location(position) {
                        return (current_idx + idx, x_perc);
                    }
                }
            }

            //Since it was not this line, add the length of it to the indexes
            current_idx += line.len();
        }

        //Since we have no index, check if we are outside
        if let Some((_line, area)) = lines.first() {
            if area.pos.1 > position.1 || area.pos.0 > position.0 {
                return (0, 0.0);
            }
        }

        if let Some((_line, area)) = lines.last() {
            if (area.pos.1 + area.extent.1) < position.1
                || (area.pos.0 + area.extent.0) < position.0
            {
                return (self.get_text().chars().count(), 0.0); //Set it on the char behind the last one
            }
        }

        //else return the last idx which will be the end then
        (current_idx, 1.0)
    }

    fn move_cursor(&self, direction: MoveDirection, lines: &Vec<(Vec<(char, Area)>, Area)>) {
        if let Selection::CursorAt(ref mut idx) =
            *self.selection.write().expect("Could not move cursor")
        {
            let mut move_len: i64 = match direction {
                MoveDirection::Left => -1,
                MoveDirection::Right => 1,
                MoveDirection::Up | MoveDirection::Down => {
                    //Find a x value of this idx at the line before...
                    //TODO implement
                    let mut current_idx = 0;
                    let (mut char_pos, mut line_pos) = (None, None);
                    for (line_idx, (line, _line_are)) in lines.iter().enumerate() {
                        for (_ch, ch_area) in line.iter() {
                            if current_idx == *idx {
                                char_pos = Some(ch_area.pos);
                                line_pos = Some(line_idx);
                                current_idx += 1;
                                break;
                            }
                            current_idx += 1;
                        }
                    }

                    if let MoveDirection::Up = direction {
                        if let (Some(lp), Some(pos)) = (line_pos, char_pos) {
                            //Check if we can go one up, otherwise we are also at the start
                            if lp >= 1 {
                                //We can go up
                                //For go to the line before and find the index at which this X_val i located
                                current_idx = 0;
                                let mut final_idx = None;
                                for (line_idx, (line, line_area)) in lines.iter().enumerate() {
                                    if line_idx == (lp - 1) {
                                        //Check if we have to go before or after that line
                                        if pos.0 < line_area.pos.0 {
                                            final_idx = Some(current_idx);
                                            break;
                                        }

                                        if pos.0 > (line_area.pos.0 + line_area.extent.0) {
                                            final_idx = Some(current_idx + line.len() - 1);
                                            break;
                                        }

                                        //Since we don't have to clamp to the left or right of this line, search for an appropriate character idx
                                        for (_ch, ch_area) in line.iter() {
                                            if ch_area.is_in((pos.0 + 1.0, ch_area.pos.1 + 1.0)) {
                                                final_idx = Some(current_idx);
                                                break;
                                            }
                                            current_idx += 1;
                                        }
                                    } else {
                                        current_idx += line.len();
                                    }
                                }

                                if let Some(id) = final_idx {
                                    /*
                                    println!(
                                        "Should go to {}, going {}, from {}",
                                        id,
                                        -(*idx as i64 - (id as i64)),
                                        idx
                                    );
                                    */
                                    -(*idx as i64 - (id as i64))
                                } else {
                                    //Go to the start
                                    /*
                                    println!("Could not find appropriat idx!");
                                    */
                                    0
                                }
                            } else {
                                0 - (*idx as i64)
                            }
                        } else {
                            //could not find line so we have to be at the start
                            0 - *idx as i64
                        }
                    } else {
                        if let (Some(lp), Some(pos)) = (line_pos, char_pos) {
                            //Check if we can go one down, otherwise we move to the end of the last line
                            if lp < lines.len() {
                                //We can go down
                                //Go to the line down from lp
                                current_idx = 0;
                                let mut final_idx = None;
                                for (line_idx, (line, line_area)) in lines.iter().enumerate() {
                                    if line_idx == (lp + 1) {
                                        /*
                                        println!("On Line {}", line_idx);
                                        */
                                        //Check if we have to go before or after that line
                                        if pos.0 < line_area.pos.0 {
                                            final_idx = Some(current_idx);
                                            break;
                                        }

                                        if pos.0 > (line_area.pos.0 + line_area.extent.0) {
                                            final_idx = Some(current_idx + line.len() - 1);
                                            break;
                                        }

                                        //Since we don't have to clamp to the left or right of this line, search for an appropriate character idx
                                        for (ch, ch_area) in line.iter() {
                                            /*
                                            println!("Checking char: {}", ch);
                                            */
                                            if ch_area.is_in((pos.0 + 1.0, ch_area.pos.1 + 1.0)) {
                                                final_idx = Some(current_idx);
                                                break;
                                            }
                                            current_idx += 1;
                                        }
                                    } else {
                                        current_idx += line.len();
                                    }
                                }
                                //FIX UP
                                if let Some(id) = final_idx {
                                    /*
                                    println!(
                                        "Should go to {}, going {}, from {}",
                                        id,
                                        (id - *idx) as i64,
                                        idx
                                    );
                                    */
                                    (id - *idx) as i64
                                } else {
                                    //Go to the end
                                    /*
                                    println!("Could not find appropriat idx!");
                                    */
                                    (self.get_text().chars().count() - *idx) as i64
                                }
                            } else {
                                (self.get_text().chars().count() - *idx) as i64
                            }
                        } else {
                            //could not find line so we have to be at the end
                            (self.get_text().chars().count() - *idx) as i64
                        }
                    }
                }
                //MoveDirection::Down => 0,
            };

            if move_len < 0 {
                if move_len.abs() > *idx as i64 {
                    move_len = -(*idx as i64);
                }
            } else {
                if move_len.abs() as usize + *idx > self.get_text().chars().count() {
                    move_len = (self.get_text().chars().count() - *idx) as i64;
                }
            }

            if move_len < 0 {
                *idx -= move_len.abs() as usize
            } else {
                *idx += move_len.abs() as usize;
            }
        }
    }

    ///Inserts a character based on the current selection
    fn insert(&self, prim: InsertPrimitves) {
        //First of all make this a valid selection
        let mut valid_selection = self.get_selection();
        valid_selection.to_character_align_selection();
        
        match valid_selection {
            Selection::NoSelection => {}
            Selection::CursorAt(idx) => {
                //Just insert at the given idx and move one in front
                let mut move_len: i64 = 0;
                match prim {
                    InsertPrimitves::String(string) => {
                        let string_index = self.selection_index_to_utf_index(idx);
                        self.raw_text
                            .write()
                            .expect("Could not insert character!")
                            .insert_str(string_index, &string);
                        move_len = string.chars().count() as i64;
                    }
                    InsertPrimitves::Delete(in_front) => {
                        if in_front {
                            //Don't remove if not possible
                            if idx <= 0 {
                                return;
                            }
                            let string_index = self.selection_index_to_utf_index(idx - 1);
                            self.raw_text
                                .write()
                                .expect("Could not remove in front of cursor")
                                .remove(string_index);
                            move_len = -1;
                        } else {
                            if idx >= self.get_text().chars().count() {
                                //Do not remove
                                return;
                            }
                            let string_index = self.selection_index_to_utf_index(idx);
                            self.raw_text
                                .write()
                                .expect("Could not remove in front of cursor")
                                .remove(string_index);
                            move_len = 0;
                        }
                    },
                    InsertPrimitves::CopyToClipboard(_should_delete) => {
                        //We do nothing since nothing is selected
                    }
                    InsertPrimitves::Nothing => {
                        return;
                    }
                }

                if let Selection::CursorAt(ref mut idx) = *self
                    .selection
                    .write()
                    .expect("Could not override cursor pos")
                {
                    if move_len < 0 {
                        if move_len.abs() > *idx as i64 {
                            move_len = -(*idx as i64);
                        }
                    } else {
                        if move_len.abs() as usize + *idx > self.get_text().chars().count() {
                            move_len = (self.get_text().chars().count() - *idx) as i64;
                        }
                    }

                    if move_len < 0 {
                        *idx -= move_len.abs() as usize
                    } else {
                        *idx += move_len.abs() as usize;
                    }
                } else {
                    println!("widkan: WARNING: failed to move cursor!");
                }
            }
            Selection::Selection {
                start,
                end,
                has_ended: _,
            } => {

                match prim{
                    InsertPrimitves::String(_) | InsertPrimitves::Delete(_) => {
                        //If anything is done, delete the currently selected part and then insert with a cursor
                        let text = self.get_text();
                        let (before_sel_and_sel_part, after_sel_part) = if end.0 + 1 == text.len() - 1 {
                            (text.as_str(), "")
                        } else {
                            text.split_at(self.selection_index_to_utf_index(end.0 + 1))
                        };

                        let (befor_sel, _sel) =
                            before_sel_and_sel_part.split_at(self.selection_index_to_utf_index(start.0));
                        let mut new_text = befor_sel.to_string();
                        new_text.push_str(after_sel_part);

                        *self
                            .raw_text
                            .write()
                            .expect("Could not override selection!") = new_text;
                        //Also override the selection with a cursor at the split location
                        *self
                            .selection
                            .write()
                            .expect("Could not override selection!") = Selection::CursorAt(start.0);
                        //Now execute whatever command it was, if it was not a delete command
                        if let InsertPrimitves::Delete(_) = prim {
                            return;
                        } else {
                            self.insert(prim);
                        }
                    },
                    InsertPrimitves::CopyToClipboard(should_delete) => {
                        let text = self.get_text();
                        let (before_sel_and_sel_part, after_sel_part) = if end.0 + 1 == text.len() - 1 {
                            (text.as_str(), "")
                        } else {
                            text.split_at(self.selection_index_to_utf_index(end.0 + 1))
                        };

                        let (befor_sel, selection) =
                            before_sel_and_sel_part.split_at(self.selection_index_to_utf_index(start.0));

                        let mut context: ClipboardContext = if let Ok(ctx) = ClipboardProvider::new(){
                            ctx
                        }else{
                            println!("Warning could not get clipboard context!");
                            return;
                        };
                        
                        //Now either just copy "selection" to the clipboard, or do this and delete the rest
                        if should_delete{

                            if let Err(er) = context.set_contents(selection.to_string()){
                                println!("widkan: WARNING: Failed to set clipboard content!");
                                println!("\tCopy error: {:?}", er);
                                return;
                            }
                            
                            let mut new_text = befor_sel.to_string();
                            new_text.push_str(after_sel_part);

                            *self
                                .raw_text
                                .write()
                                .expect("Could not override selection!") = new_text;
                            //Also override the selection with a cursor at the split location
                            *self
                                .selection
                                .write()
                                .expect("Could not override selection!") = Selection::CursorAt(start.0);
                        }else{
                            if let Err(_er) = context.set_contents(selection.to_string()){
                                println!("widkan: WARNING: Failed to set clipboard content!");
                                return;
                            }
                        }
                        
                    },
                    InsertPrimitves::Nothing => {}, //Do nothing
                }
                
            }
        }
    }
}

impl Widget for TextBox {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {

        let area = self.get_area();
        //tracks the level at which we are drawing, since maybe a "marking" rect is added.
        let current_level = level;
        //Now draw all lines we get from our box
        let lines = self.get_text_area(renderer);
        let mut selection = self.get_selection();
        //Collects the rendering calls
        let mut render_calls = Vec::new();

        //First of all, add the background
        render_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            area,
            current_level,
            DrawType::Theme(Theme::Background),
        ));
        
        //We validate the selection to have an valid selection area
        selection.to_character_align_selection();
        //Also check if we are currently a cursor
        if let Selection::Selection {
            start,
            end,
            has_ended: _,
        } = selection
        {
            if start.0 == end.0 && start.1 == end.1 {
                let cursor_selection = if start.1 > 0.5 { start.0 + 1 } else { start.0 };
                selection = Selection::CursorAt(cursor_selection);
            }
        }

        let mut current_char_idx = 0;

        for (line, line_area) in lines.into_iter() {
            let mut print_string = String::with_capacity(line.len());
            //the possible selection area for this line
            let mut selection_area: Option<Area> = None;
            let mut local_level = current_level + 1;
            let mut last_char_area = Area::empty();
            for (ch, ch_area) in line {
                print_string.push(ch);

                //Check if the current char is in a selection, if so add it to the selection area
                match selection {
                    Selection::Selection {
                        start,
                        end,
                        has_ended: _,
                    } => {
                        if current_char_idx >= start.0 && current_char_idx <= end.0 {
                            if let Some(ref mut line_selection) = selection_area {
                                //Update the line selection area with our extent
                                line_selection.extent.0 += ch_area.extent.0;
                            } else {
                                selection_area = Some(ch_area);
                            }
                        }
                    }
                    Selection::CursorAt(idx) => {
                        //Print the cursor here and add the local level
                        if idx == current_char_idx {
                            let cursor_area = Area {
                                pos: (
                                    ch_area.pos.0 - (self.cursor_width / 2.0) as f64,
                                    ch_area.pos.1,
                                ),
                                extent: (self.cursor_width as f64, ch_area.extent.1),
                            };

                            render_calls.push(PrimitiveCall::new(
                                Primitive::Rect,
                                cursor_area,
                                local_level,
                                DrawType::Theme(Theme::TextCursor),
                            ));
                            local_level += 1;
                        }
                    }
                    _ => {}
                }

                //Finally add char idx for the next one
                current_char_idx += 1;
                last_char_area = ch_area;
            }

            if let Some(sel_area) = selection_area {
                render_calls.push(PrimitiveCall::new(
                    Primitive::Rect,
                    sel_area,
                    local_level,
                    DrawType::Theme(Theme::TextHighlight),
                ));

                local_level += 1;
            }

            //Render the cursor behind the last character if needed
            if let Selection::CursorAt(idx) = selection {
                //Print the cursor here and add the local level
                if idx == current_char_idx {
                    let cursor_area = Area {
                        pos: (
                            last_char_area.pos.0 + last_char_area.extent.0 - (self.cursor_width / 2.0) as f64,
                            last_char_area.pos.1,
                        ),
                        extent: (self.cursor_width as f64, last_char_area.extent.1),
                    };

                    render_calls.push(PrimitiveCall::new(
                        Primitive::Rect,
                        cursor_area,
                        local_level,
                        DrawType::Theme(Theme::TextCursor),
                    ));
                    local_level += 1;
                }
            }
            
            //Render the string
            render_calls.push(PrimitiveCall::new(
                Primitive::Text {
                    content: print_string,
                    size: self.size,
                },
                line_area,
                local_level,
                DrawType::Theme(Theme::Text),
            ));
        }

        render_calls
    }

    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {
        //The current line assembly
        let mut lines = self.get_text_area(renderer);
        let mut current_selection = self.get_selection();
        let mut next_key_ctrl_key = None;
       
        for ev in events {
            match ev.event {
                EventType::MouseClick {
                    state,
                    button,
                    modifiers: _,
                } => {
                    if self.selectable {
                        if button == MouseButton::Left {
                            if state == ElementState::Pressed {
                                //Check if the click was within our area, if not we just ignore it
                                if self.get_area().is_in(ev.location) {
                                    //Start a new selection by setting start and end the same
                                    let selection_info =
                                        self.index_for_position(ev.location, &lines);
                                    current_selection = Selection::Selection {
                                        start: selection_info,
                                        end: selection_info,
                                        has_ended: false,
                                    };
                                } else {
                                    //Reset the selection since someone clicked somewhere
                                    current_selection = Selection::NoSelection;
                                }
                            } else {
                                //The cursor was release, check if the release location is the same as the press location, if so convert to a "selection",
                                match current_selection {
                                    Selection::NoSelection => {}
                                    Selection::Selection {
                                        start,
                                        end,
                                        has_ended,
                                    } => {
                                        //Check if this selection has not finished yet
                                        if !has_ended {
                                            //This will happen when the cursor is set to CursorAt
                                            if start.0 == end.0 && start.1 == end.1 {
                                                let real_index = if start.1 > 0.5 {
                                                    start.0 + 1
                                                } else {
                                                    start.0
                                                };

                                                current_selection = Selection::CursorAt(real_index);
                                            } else {
                                                //A real (multi)line selection has finished, set it accordingly
                                                current_selection = Selection::Selection {
                                                    start,
                                                    end,
                                                    has_ended: true,
                                                }
                                            }
                                        }
                                    }
                                    Selection::CursorAt(_cursor_idx) => {}
                                }
                            }
                        }
                    }
                }
                EventType::CursorMoved {
                    position: _,
                    delta: _,
                    modifiers: _,
                } => {
                    //Check if we are currently selection something, if so update the end position
                    if let Selection::Selection {
                        start: _,
                        ref mut end,
                        has_ended,
                    } = current_selection
                    {
                        if !has_ended {
                            let new_end_info = self.index_for_position(ev.location, &lines);
                            *end = new_end_info;
                        }
                    }
                }
                EventType::KeyInput(input) => {
                    if self.get_area().is_in(ev.location) {
                        if let Some(keycode) = input.virtual_keycode {
                            if input.state == ElementState::Pressed {
                                match keycode {
                                    VirtualKeyCode::Space => {
                                        next_key_ctrl_key = Some(InsertPrimitves::String(" ".to_string()));
                                    }
                                    VirtualKeyCode::Return => {
                                        //println!("Return");
                                        next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                        //Do not insert atm
                                    }
                                    VirtualKeyCode::NumpadEnter => {
                                        //Currently don't do anything
                                        next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                    }
                                    VirtualKeyCode::Numlock => {
                                        //Currently don't do anything
                                        next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                    }
                                    VirtualKeyCode::Delete => {
                                        //println!("Delete");
                                        next_key_ctrl_key = Some(InsertPrimitves::Delete(false));
                                    }

                                    VirtualKeyCode::Back => {
                                        //println!("Back");
                                        next_key_ctrl_key = Some(InsertPrimitves::Delete(true));
                                    }
                                    VirtualKeyCode::Tab => {
                                        //Currently don't do anything
                                        next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                    }
                                    VirtualKeyCode::Escape => {
                                        next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                    }
                                    VirtualKeyCode::Left => {
                                        self.move_cursor(MoveDirection::Left, &lines);
                                        current_selection = self.get_selection();
                                    }
                                    VirtualKeyCode::Right => {
                                        self.move_cursor(MoveDirection::Right, &lines);
                                        current_selection = self.get_selection();
                                    }
                                    VirtualKeyCode::Up => {
                                        self.move_cursor(MoveDirection::Up, &lines);
                                        current_selection = self.get_selection();
                                    }
                                    VirtualKeyCode::Down => {
                                        self.move_cursor(MoveDirection::Down, &lines);
                                        current_selection = self.get_selection();
                                    },
                                    VirtualKeyCode::V => {
                                        //Ctrl+C was pressed
                                        if input.modifiers.ctrl{
                                            let mut context: ClipboardContext = if let Ok(ctx) = ClipboardProvider::new(){
                                                ctx
                                            }else{
                                                println!("Warning could not get clipboard context!");
                                                next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                                continue;
                                            };
                                            let content = if let Ok(c) = context.get_contents(){
                                                c
                                            }else{
                                                println!("widkan: WARNING: could not get clipboard content!");
                                                next_key_ctrl_key = Some(InsertPrimitves::Nothing);
                                                continue
                                            };
                                            //Since we got an context, set it
                                            next_key_ctrl_key = Some(InsertPrimitves::String(content));
                                        }
                                    },
                                    VirtualKeyCode::C => {
                                        if input.modifiers.ctrl{
                                            next_key_ctrl_key = Some(InsertPrimitves::CopyToClipboard(false))
                                        }
                                    },
                                    VirtualKeyCode::X => {
                                        if input.modifiers.ctrl{
                                            next_key_ctrl_key = Some(InsertPrimitves::CopyToClipboard(true))
                                        }
                                    },
                                    _ => {}
                                }
                            }
                        }
                    }
                }
                EventType::ReciveCharacter(c) => {
                    //println!("Character: {:?}", c);
                    //TODO check for controll characters
                    if self.editable {
                        //Ignore this character since it messes up our formating :(
                        if let Some(ctr) = next_key_ctrl_key {
                            self.insert(ctr);
                            next_key_ctrl_key = None;
                        } else {
                            if self.character_white_list.contains(c) {
                                self.insert(InsertPrimitves::String(c.to_string()));
                            }
                        }
                        //Now update the lines and the current selection for following events
                        current_selection = self.get_selection();
                        lines = self.get_text_area(&renderer);
                    }
                }
                _ => {}
            }
        }

        //Change the inner area
        *self.area.write().expect("Failed to set labels area") = new_area;
        *self.selection.write().expect("Could not update selection") = current_selection;
        Default::default()
    }
}
