/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::rendering::primitive::*;
use crate::widget::*;

use std::sync::{Arc, RwLock};

/// Only displays the given icon within this widget. Is good to be used when layouting new
/// Widgets while using the LayoutBox.
pub struct Wicon{
    area: RwLock<Area>,
    icon: Ico,
}

impl Wicon{
    pub fn new(icon: Ico) -> Arc<Self>{
        Arc::new(
            Wicon{
                area: RwLock::new(Area::empty()),
                icon
            }
        )
    }

    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not read current area on Wicon")
    }
}

impl Widget for Wicon{
    
    fn render(&self, _renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        vec![
            PrimitiveCall::new(
                Primitive::Icon(self.icon),
                self.get_area(),
                level,
                DrawType::Theme(Theme::Pattern) //Ignored
            )
        ]
    }

    fn update(&self, new_area: Area, _events: &Vec<Event>, _renderer: &Renderer) -> UpdateRetState{
        *self.area.write().expect("Could not override Wicons area") = new_area;
        UpdateRetState::default()
    }
}
