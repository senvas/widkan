/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/// A helper widget which draws a frame around an inner child.
pub mod frame;
pub use frame::*;
///Displays text either as a single line or multi line within a given area.
/// Supports copy and paste.
pub mod text_box;
pub use text_box::*;

///Creates an widget that does not obey the new_region when updated. Instead it allocate as much space as needed in the y direction.
/// This widget is used for the ListView as ItemStorage.
pub mod list;
pub use list::*;

///Only shows the specified widget. Good when you want to embed a simple icon in a widget tree.
pub mod wicon;
pub use wicon::*;
