/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Widget
//! Defines the widget trait which each widget has to implement, as well as helper structs and defualt widgets like:
//!
//! - Box
//! - Line
//! - Image
//! - Text

use crate::rendering::*;
use crate::rendering::primitive::PrimitiveCall;
use marp::ash::vk;
use msw::winit;

use std::sync::mpsc::{Sender, Receiver, channel};

pub use crate::rendering::icon::Ico;

pub mod events;
use events::*;

///Contains helper widgets like frames or a text box.
pub mod helper;
///Contains the standart io widgets like buttons, labels and text_entrys
pub mod io;
///Contains layout related widgets which usually only manage arrangement of sub widgets.
pub mod layout;

/// Define a region in this interface where the upper left of this
/// Interface is (0.0, 0.0) and the pos/extent are given in pixels.
#[derive(Clone, Copy, Debug)]
pub struct Area {
    pub pos: (f64, f64),
    pub extent: (f64, f64),
}

impl PartialEq for Area {
    fn eq(&self, other: &Area) -> bool {
        if self.pos.0 == other.pos.0
            && self.pos.1 == other.pos.1
            && self.extent.0 == other.extent.0
            && self.extent.1 == other.extent.1
        {
            true
        } else {
            false
        }
    }
}

impl Area {
    //Creates an area at (0,0) with no extent
    pub fn empty() -> Self {
        Area {
            pos: (0.0, 0.0),
            extent: (0.0, 0.0),
        }
    }

    ///Converts the Area to a 2d transform matrix, which transforms a screen covering rectangle to a `area` covering rectangle
    pub fn to_transform(&self, screen: &Area) -> [[f32; 4]; 4] {
        //First find the scaling factor for x and y, then add the translation from the pos attribute
        let sx = self.extent.0 / screen.extent.0 * 2.0;
        let sy = self.extent.1 / screen.extent.1 * 2.0;
        //The translation in vulkan is selfs translation / screen_width * 2 since
        //vulkans coords are from -1 till 1
        let tx = (self.pos.0 / screen.extent.0 * 2.0) - 1.0;
        let ty = (self.pos.1 / screen.extent.1 * 2.0) - 1.0;

        //Build the transform matrix, glsl seams to be column/row
        [
            [sx as f32, 0.0, 0.0, 0.0],
            [0.0, sy as f32, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [tx as f32, ty as f32, 0.0, 1.0],
        ]
    }

    ///Returns true if `location` is within the area
    pub fn is_in(&self, location: (f64, f64)) -> bool {
        if location.0 > self.pos.0
            && location.0 < (self.pos.0 + self.extent.0)
            && location.1 > self.pos.1
            && location.1 < (self.pos.1 + self.extent.1)
        {
            true
        } else {
            false
        }
    }

    //Returns Some if there was an hit and None if there wheren't. If there was some the floats indicate the persentage on the x and y axis
    // "where on self" the hit was
    pub fn hit_location(&self, location: (f64, f64)) -> Option<(f64, f64)> {
        if location.0 > self.pos.0
            && location.0 < (self.pos.0 + self.extent.0)
            && location.1 > self.pos.1
            && location.1 < (self.pos.1 + self.extent.1)
        {
            Some((
                (location.0 - self.pos.0) / self.extent.0,
                (location.1 - self.pos.1) / self.extent.1,
            ))
        } else {
            None
        }
    }

    ///Returns the scissors rectangle needed if the renderer should only draw within this area
    pub fn to_scissors(&self) -> vk::Rect2D {
        vk::Rect2D {
            offset: vk::Offset2D {
                x: self.pos.0.floor() as i32,
                y: self.pos.1.floor() as i32,
            },
            extent: vk::Extent2D {
                width: self.extent.0.max(0.0) as u32,
                height: self.extent.1.max(0.0) as u32,
            },
        }
    }

    pub fn has_zero_dimension(&self) -> bool {
        if self.extent.0 < 0.0 || self.extent.1 < 0.0 {
            true
        } else {
            false
        }
    }
    ///Clamps self in a way that it never exceeds `host`
    pub fn clamp_within(&mut self, host: &Area) {
        self.pos = (self.pos.0.max(host.pos.0), self.pos.1.max(host.pos.1));
        let max_extent_for_pos = (
            (host.pos.0 + host.extent.0) - self.pos.0,
            (host.pos.1 + host.extent.1) - self.pos.1,
        );

        self.extent = (
            self.extent.0.min(max_extent_for_pos.0),
            self.extent.1.min(max_extent_for_pos.1),
        );
    }

    ///Returns an area which is `border_width` moved into this area and has cut away two times as much.
    /// In short if you draw something with host area first, and then something with this outcome, you'll have a border line
    /// in the color of host.
    pub fn from_frame(&self, border_width: u32) -> Area{
        Area {
            pos: (
                (self.pos.0 + border_width as f64),
                (self.pos.1 + border_width as f64),
            ),
            extent: (
                (self.extent.0 - (2.0 * border_width as f64)),
                (self.extent.1 - (2.0 * border_width as f64)),
            )
        }
    }
}



///Describes how the text is aligned within its area.
pub enum Alignment {
    Center,
    UpLeft,
    UpRight,
    LowLeft,
    LowRight,
}

impl Alignment {
    ///Aligns `to_align` in a way that it is moved to the given position of `self`.
    /// # Safety
    /// it is not checked that `to_align` fits into `host`. So if you chose center alignment and `to_align` is wider
    /// then the host, it will peek out on both sides.
    pub fn to_aligned_area(&self, host: Area, to_align: Area) -> Area {
        let aligned = match self {
            Alignment::Center => Area {
                pos: (
                    host.pos.0 + ((host.extent.0 / 2.0) - (to_align.extent.0 / 2.0)),
                    host.pos.1 + ((host.extent.1 / 2.0) - (to_align.extent.1 / 2.0)),
                ),
                extent: to_align.extent,
            },
            Alignment::UpLeft => Area {
                pos: host.pos,
                extent: to_align.extent,
            },
            Alignment::UpRight => Area {
                pos: (host.pos.0 + host.extent.0 - to_align.extent.0, host.pos.1),
                extent: to_align.extent,
            },
            Alignment::LowLeft => Area {
                pos: (host.pos.0, host.pos.1 + host.extent.1 - to_align.extent.1),
                extent: to_align.extent,
            },
            Alignment::LowRight => Area {
                pos: (
                    host.pos.0 + host.extent.0 - to_align.extent.0,
                    host.pos.1 + host.extent.1 - to_align.extent.1,
                ),
                extent: to_align.extent,
            },
        };

        //Check that the aligned values area non negative etc.
        Area {
            pos: (aligned.pos.0.max(host.pos.0), aligned.pos.1.max(host.pos.1)),
            extent: (
                aligned.extent.0.min(host.extent.0),
                aligned.extent.1.min(host.extent.1),
            ),
        }
    }
}

///A Struct which can be filled with state information which has to be fulfilled by the caller of an `update()` on a widget tree.
#[derive(Clone, Copy, Debug)]
pub struct UpdateRetState {
    pub new_cursor: winit::MouseCursor,
}

impl Default for UpdateRetState {
    fn default() -> Self {
        UpdateRetState {
            new_cursor: winit::MouseCursor::Default,
        }
    }
}

impl UpdateRetState {
    ///Merges changed states from other into self. Prefers `others`state if self and `others` state are both changed (from the default value)
    pub fn merge(&self, other: UpdateRetState) -> UpdateRetState {
        UpdateRetState {
            new_cursor: if other.new_cursor != winit::MouseCursor::Default {
                other.new_cursor
            } else {
                self.new_cursor
            },
        }
    }
}

pub trait Widget {
    /// Should render add its own rendering calls to the calls of its children.
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>;
    /// Updates self according to the given events.
    ///
    /// `area` hold the area this widget has to occupy.
    /// `events` Holds all events.
    ///
    /// # Safety
    /// If you implement this trait your self, the updating should be handled as described above. In addition the update mechanism should
    /// call the update function with the appropriate parameters for all its children to gurantee correct rendering. Events are handled in the
    /// order they occur. `new_area` is the initial size. However, if there is a Resize event, all following events are in respect to the new area.
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState;
}
