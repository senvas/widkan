/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use std::path::*;

use msw::winit;
use msw::winit::VirtualKeyCode;
use msw::winit::WindowEvent;

///Some soft wrapper around winits Events which mostly converts the locations from pixel format to the
/// format of `Area` to make intersection tests easier.
#[derive(Clone, Debug)]
pub enum EventType {
    Awaken,
    Suspend(bool),
    /// When a resize occurs. The event caries the new target area for the widget.
    Resize(Area),
    /// When the window was moved. Since the window location does not applies to the interface toolkit,
    /// this mostly does need to be handled. The location is in pixels on the screen.
    Moved((f64, f64)),
    ///When a closing event was send to the window
    Close,
    ///When the window has been destroyed.
    Destroy,
    ReciveCharacter(char),
    ///When a File has been dropped into the window.
    FileDrop(PathBuf),
    HoverFile(PathBuf),
    HoverCancel,
    Focused(bool),
    KeyInput(winit::KeyboardInput),
    ///When the cursor moved within the window bounds
    CursorMoved {
        ///The position relative to the top left of this window, the position is given in pixels.
        position: (f64, f64),
        ///The delta movement in pixel
        delta: (f64, f64),
        modifiers: winit::ModifiersState,
    },
    /// The Raw physical motion of this mouse device. Should not be used as cursor input, more if you want to control for instance a game camera with it.
    MouseMotion {
        delta: (f64, f64),
    },
    CursorEntered,
    CursorLeft,
    MouseWheel {
        //How much in each axis was scrolled. Positive is away from user/right.
        delta: (f64, f64),
        modifiers: winit::ModifiersState,
    },
    MouseClick {
        state: winit::ElementState,
        button: winit::MouseButton,
        modifiers: winit::ModifiersState,
    },
    //TODO At this point I left out TouchpadPressure events which are only supported on some apple devices as well as axis events.
    ///When the window needs to be redrawn. The general redrawing will be handled by the renderer, however, if you need ton detect
    Refresh,
    Touch(winit::Touch),
    HiDpiFactorChanged(f64),
    ///Some button was touched, must not be a mouse or keyboard button.
    Button {
        id: winit::ButtonId,
        state: winit::ElementState,
    },
}

#[derive(Clone, Debug)]
pub struct Event {
    /// The location at which the cursor was when the event happened. Similar to `Area` it is a location relative to the top left.
    /// The location is given in pixels.
    pub location: (f64, f64),
    pub event: EventType,
}

impl Event{
    pub fn to_winit_event(&self) -> winit::Event{
        let id = unsafe{winit::WindowId::dummy()};
        let d_id = unsafe{winit::DeviceId::dummy()};
        
        match self.event{
            EventType::Awaken => winit::Event::Awakened,
            EventType::Suspend(is_suspended) => winit::Event::Suspended(is_suspended),
            EventType::Resize(area) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Resized(winit::dpi::LogicalSize::new(area.extent.0, area.extent.1))},
            EventType::Moved(to) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Resized(winit::dpi::LogicalSize::new(to.0, to.1))},
            EventType::Close => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::CloseRequested},
            EventType::Destroy => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Destroyed},
            EventType::ReciveCharacter(ch) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::ReceivedCharacter(ch)},
            EventType::FileDrop(ref path) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::DroppedFile(path.clone())},
            EventType::HoverFile(ref path) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::HoveredFile(path.clone())},
            EventType::HoverCancel => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::HoveredFileCancelled},
            EventType::Focused(focused) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Focused(focused)},
            EventType::KeyInput(input) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::KeyboardInput{device_id: d_id, input}},
            EventType::CursorMoved{position, delta, modifiers} => winit::Event::WindowEvent{
                window_id: id,
                event: winit::WindowEvent::CursorMoved{
                    device_id: d_id,
                    position: winit::dpi::LogicalPosition::new(position.0, position.1),
                    modifiers
                }
            },
            EventType::MouseMotion{delta} => winit::Event::DeviceEvent{device_id: d_id, event: winit::DeviceEvent::MouseMotion{delta}},
            EventType::CursorEntered => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::CursorEntered{device_id: d_id}},
            EventType::CursorLeft => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::CursorLeft{device_id: d_id}},
            EventType::MouseWheel{delta, modifiers} => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::MouseWheel{
                device_id: d_id,
                delta: winit::MouseScrollDelta::LineDelta(delta.0 as f32, delta.1 as f32),
                phase: winit::TouchPhase::Moved, modifiers
            }},
            EventType::MouseClick{state, button, modifiers} => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::MouseInput{
                device_id: d_id,
                state,
                button,
                modifiers
            }},
            EventType::Refresh => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Refresh},
            EventType::Touch(touch) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::Touch(touch)},
            EventType::HiDpiFactorChanged(new_factor) => winit::Event::WindowEvent{window_id: id, event: winit::WindowEvent::HiDpiFactorChanged(new_factor)},
            EventType::Button{id, state} => winit::Event::DeviceEvent{device_id: d_id, event: winit::DeviceEvent::Button{button: id, state}}
        }
    }
}

///Takes a collection of winit-events as well as an entry cursor location and generates a list of
/// `Event`s from them. Returns a list of Events as well as the last cursor location after all events have taken place.
pub fn to_events(
    start_cursor_position: (f64, f64),
    events: Vec<winit::Event>,
    window: &winit::Window,
) -> (Vec<Event>, (f64, f64)) {
    //Tracks the cursor location
    let mut cursor_location = start_cursor_position;
    let mut fin_events: Vec<Event> = Vec::with_capacity(events.len());

    //tracks the dpi value which is needed to convert the logical sizes given by winit
    //into actual pixels
    let mut dpi = window.get_hidpi_factor() as f64;

    for ev in events.into_iter() {
        match ev {
            winit::Event::WindowEvent { window_id: _, event } => {
                match event {
                    WindowEvent::Resized(new_size) => {
                        let physical_size = {
                            let intermediate: (f64, f64) = new_size.into();

                            (intermediate.0 * dpi, intermediate.1 * dpi)
                        };

                        fin_events.push(Event {
                            location: cursor_location,
                            event: EventType::Resize(Area {
                                pos: (0.0, 0.0),
                                extent: physical_size,
                            }),
                        })
                    }
                    WindowEvent::Moved(new_location) => {
                        let physical_loc = {
                            let intermediate: (f64, f64) = new_location.into();

                            (intermediate.0 * dpi, intermediate.1 * dpi)
                        };

                        fin_events.push(Event {
                            location: cursor_location,
                            event: EventType::Moved(physical_loc),
                        })
                    }
                    WindowEvent::CloseRequested => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Close,
                    }),
                    WindowEvent::Destroyed => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Destroy,
                    }),
                    WindowEvent::DroppedFile(path) => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::FileDrop(path),
                    }),
                    WindowEvent::HoveredFile(path) => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::HoverFile(path),
                    }),
                    WindowEvent::HoveredFileCancelled => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::HoverCancel,
                    }),
                    WindowEvent::ReceivedCharacter(ch) => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::ReciveCharacter(ch),
                    }),
                    WindowEvent::Focused(b) => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Focused(b),
                    }),
                    WindowEvent::KeyboardInput { device_id: _, input } => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::KeyInput(input),
                    }),
                    WindowEvent::CursorMoved {
                        device_id: _,
                        position,
                        modifiers,
                    } => {
                        let physical_loc = {
                            let intermediate: (f64, f64) = position.into();

                            (intermediate.0 * dpi, intermediate.1 * dpi)
                        };

                        let delta = {
                            (
                                physical_loc.0 - cursor_location.0,
                                physical_loc.1 - cursor_location.1,
                            )
                        };

                        //Update events, then set the "current mouse position"
                        fin_events.push(Event {
                            location: cursor_location,
                            event: EventType::CursorMoved {
                                position: physical_loc,
                                delta,
                                modifiers: modifiers,
                            },
                        });
                        cursor_location = physical_loc;
                    }
                    WindowEvent::CursorEntered { device_id: _ } => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::CursorEntered,
                    }),
                    WindowEvent::CursorLeft { device_id: _ } => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::CursorLeft,
                    }),
                    WindowEvent::MouseWheel {
                        device_id: _,
                        delta,
                        phase: _,
                        modifiers,
                    } => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::MouseWheel {
                            delta: match delta {
                                winit::MouseScrollDelta::LineDelta(x, y) => (x as f64, y as f64),
                                winit::MouseScrollDelta::PixelDelta(pos) => pos.into(),
                            },
                            modifiers,
                        },
                    }),
                    WindowEvent::MouseInput {
                        device_id: _,
                        state,
                        button,
                        modifiers,
                    } => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::MouseClick {
                            state,
                            button,
                            modifiers,
                        },
                    }),
                    WindowEvent::Refresh => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Refresh,
                    }),
                    WindowEvent::Touch(t) => fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Touch(t),
                    }),
                    WindowEvent::HiDpiFactorChanged(f) => {
                        //Update the cursor location, dpi factor and issue a resize event since we have to change the physical size
                        let logical_cursor_pos = (cursor_location.0 / dpi, cursor_location.1 / dpi);
                        let physical_curosr_pos = (
                            logical_cursor_pos.0 * f as f64,
                            logical_cursor_pos.1 * f as f64,
                        );

                        cursor_location = physical_curosr_pos;

                        fin_events.push(Event {
                            location: cursor_location,
                            event: EventType::HiDpiFactorChanged(f),
                        });

                        let new_size: (f64, f64) = window
                            .get_inner_size()
                            .expect("Failed to get current inner window size")
                            .into();
                        fin_events.push(Event {
                            location: cursor_location,
                            event: EventType::Resize(Area {
                                pos: (0.0, 0.0),
                                extent: (new_size.0 * f, new_size.1 * f),
                            }),
                        });

                        //Finally update dpi
                        dpi = f;
                    }
                    _ => {} //println!("widkan: Warning, unhandled input"),
                }
            }
            winit::Event::DeviceEvent { device_id: _, event } => {
                match event{
                    winit::DeviceEvent::MouseMotion{delta} => fin_events.push(Event{
                        location: cursor_location,
                        event: EventType::MouseMotion{delta}
                    }),
                    _ => {} //println!("widkan: Device Events are currently not handled"),
                }
            }
            winit::Event::Awakened => fin_events.push(Event {
                location: cursor_location,
                event: EventType::Awaken,
            }),
            winit::Event::Suspended(s) => fin_events.push(Event {
                location: cursor_location,
                event: EventType::Suspend(s),
            }),
        }
    }

    (fin_events, cursor_location)
}

pub fn key_code_to_char(code: VirtualKeyCode, is_capital: bool) -> Option<char> {
    match code {
        VirtualKeyCode::A => {
            if is_capital {
                Some('A')
            } else {
                Some('a')
            }
        }
        _ => None,
    }
}
