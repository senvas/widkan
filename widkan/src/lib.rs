/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Widkan
//! TODO do some general documentation...

///For image/icon loading
pub extern crate image;
///The vulkan wrapper
pub extern crate marp;
///The winit-marp clue, also provides winit
pub extern crate marp_surface_winit as msw;
///The config format we use.
pub extern crate ron;
///For font loading and pushing and generating the gpu side character look-up texture.
pub extern crate rusttype;
///Serializing and deserializing the config data
pub extern crate serde;
///For getting and setting clipboard content
pub extern crate clipboard;


///Contains the Configuration Struct which controlls the interfaces
/// theming as well as some technical details.
pub mod config;
///Contains the main entry point for the library, the `Interface`
pub mod interface;
///Contains the renderer as well as the primitives which can be drawn
pub mod rendering;
///Contains the standard widgets as well as event handling.
pub mod widget;
