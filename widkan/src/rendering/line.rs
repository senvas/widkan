/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use marp::buffer::*;
use marp::command_buffer::*;
use marp::descriptor::*;
use marp::device::*;
use marp::framebuffer::RenderPass;
use marp::memory::*;
use marp::pipeline::*;
use marp::shader::*;

use marp::ash::vk;

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::rendering::Renderer;
use std::sync::Arc;

use std::u64;



fn default_line_vertex_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/line/vert.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read line shader!")
}

fn default_line_frag_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/line/frag.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read line shader!")
}


///A line can contain several points which make up the line.
///Currently no iterpolation is handled.
/// The coordinates are relative to the `area` supplied
/// to the `PrimitiveCall` when adding the draw call to the renderer.
pub struct Line {
    device: Arc<Device>,
    ///The pipeline that renders a single rectangle.
    pipeline: Arc<GraphicsPipeline>,
}

impl Line {
    ///Creates a new Rectangle which can be drawn when a command buffer is supplied
    pub fn new(
        config: &Config,
        device: Arc<Device>,
        _init_queue: Arc<Queue>,
        render_pass: Arc<RenderPass>,
    ) -> Self {
        //Create the pipeline that handles the quad drawing.
        //Load Shaders
        let vertex_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
                device.clone(),
                &(config.shader_directory.clone() + "line/vert.spv"),
            ).expect("Failed to load vertex shader: line")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_line_vertex_shader()
            ).expect("Failed to load default line shader!")
        };
        
        let fragment_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
            device.clone(),
            &(config.shader_directory.clone() + "line/frag.spv"),
        ).expect("Failed to load fragment shader: line")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_line_frag_shader()
            ).expect("Failed to load default line shader!")
        };
            

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        // setup Graphicspipeline
        //This one descibes how vertices are bound to the shader.
        let vertex_state = get_default_vertex_state();
        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = InputAssemblyState {
            topology: vk::PrimitiveTopology::LINE_STRIP,
            restart_enabled: false,
        };

        //raster_info
        let raster_info = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //TODO multisample_state
        let multisample_state = MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //TODO depth_stencil_state
        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };
        //Blend with everything we get
        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            Some(vk::LogicOp::COPY),
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::NoBlending,
            ],
        );

        //Create pipeline
        let pipeline_state = PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            ViewportState::new(ViewportMode::Dynamic(1)), //We have no viewport state since we set the viewport at runtime
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );

        //Create a fake push constant to have some range
        let fake_push = PushConstant::new(
            LinePush {
                transform: [[0.0;4];4],
                level: 1.0,
                color: [1.0;4],
                pad: [0.0;3]
            },
            vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
        );

        //Setup the persistent descriptor sets as well as the final pipeline layout, all of the primitives will use
        let pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            vec![],
            vec![*fake_push.range()],
        )
        .expect("Failed to create GraphicsPipelineLayout for Rectangle pipeline");
        let pipeline = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass,
            0, //Use subpass 0
        )
        .expect("Failed to create Graphics pipeline");
        Line {
            device,
            pipeline,
        }
    }

    pub fn get_pipeline_layout(&self) -> Arc<PipelineLayout> {
        self.pipeline.layout()
    }
}

impl PrimDrawable for Line {
    fn draw(&self, command_buffer: Arc<CommandBuffer>, calls: Vec<PrimitiveCall>, renderer: &Renderer) {
        //First bind the pipeline, index buffer and vertex buffer, then draw all calls.

        //Extract all information. For each line, build a vertex buffer within this command buffer.
        //                   color   level width point-coords     VertexBuffer
        let call_infos: Vec<(PrimitiveCall, [f32;4], f32, f32, Vec<(f64, f64)>, Arc<Buffer>)> = calls.into_iter().filter_map(|c|{
            if let Primitive::Line{point_set, line_width, color} = c.clone().prim {

                let vertices: Vec<_> = point_set.iter().map(|p| Vertex {
                    pos: [p.0 as f32, p.1 as f32, 0.0, 1.0],
                    uv: [0.0, 1.0],
                }).collect();
                //We use RAM here since its the fastest to map and we won't have so much data usually.
                let (upload, buffer) = Buffer::from_data(
                    self.device.clone(),
                    command_buffer.get_queue(),
                    vertices,
                    BufferUsage {
                        vertex_buffer: true,
                        ..Default::default()
                    },
                    SharingMode::Exclusive,
                    MemoryUsage::CpuToGpu
                ).expect("Failed to map line vertex buffer!");
                upload.wait(u64::MAX).expect("Failed to wait for line primitive vertex upload");
                let level = c.get_level();
                Some((c, color, level, line_width, point_set, buffer))
            }else{
                None
            }
        }).collect();

        
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind Rect pipeline!");

        
        for (call, color, level, width, points, buffer) in call_infos{
            //First bind the created vertex buffer
            command_buffer
                .cmd_bind_vertex_buffers(0, vec![(buffer, 0)])
                .expect("Failed to bind vertex buffer");

            //Setup the scissors for this call
            
            command_buffer
                .cmd_set_scissors(vec![call.get_scissors().to_scissors()])
                .expect("Failed to set scissors!");
            
            //Setup the line width
            command_buffer.cmd_set_line_width(width).expect("Failed to set line width!");
            
            let pc_vert = PushConstant::new(
                LinePush {
                    transform: call.get_transform(renderer.screen_area()),
                    color: color,
                    level: level,
                    pad: [0.0; 3]
                },
                vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
            );

            //Bind the current correct push const
            command_buffer
                .cmd_push_constants(self.pipeline.layout(), &pc_vert)
                .expect("Failed to push triangles constants");

            command_buffer
                .cmd_draw(points.len() as u32, 1, 0, 0)
                .expect("Failed to draw rect!");
        }
    }
}

///The default vertex buffer push block
#[repr(C)]
pub struct LinePush {
    pub transform: [[f32;4];4],
    pub color: [f32;4],
    pub level: f32,
    pub pad: [f32;3]
}
