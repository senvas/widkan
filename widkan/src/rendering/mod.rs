/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Rendering
//! Holds all rendering related objects. Most importantly the renderer itself, but also the modules for the single
//! primitives.
//! # Resource bindings
//! - 2. Dynamic DescritorSet:
//! - 2.1 binding=0: holds a texture atlas for characters. Might be regenerated when more characters are needed.
//! - 2.2 binding=1: holds the icon set

use marp;
use marp::ash::vk;
use marp::buffer::*;
use marp::command_buffer::CommandBuffer;
use marp::descriptor::*;
use marp::device::*;
use marp::image::*;
use marp::memory::MemoryUsage;


use std::sync::Arc;

use std::collections::BTreeMap;

use crate::widget::*;
pub mod primitive;
use primitive::*;
pub mod rectangle;
use rectangle::*;
pub mod icon;
use icon::*;
pub mod image;
use self::image::*;
pub mod line;
use line::*;
pub mod text;
use crate::config::Config;
use text::*;

///Describes the static data used when drawing.
#[derive(Clone, Debug)]
#[repr(C)]
pub struct ColorPallet{
    ///Base color of the background surface
    pub background_color: [f32;4],
    ///Base color for text related colors.
    pub text_color: [f32;4],
    ///Base color for highlighting
    pub highlight_color: [f32;4],
    ///Base color for interface edges of any kind,
    pub edge_color: [f32;4],
    pub inc: f32,
}


///Some utility functions to change base colors to special version
trait ColorTricks{
    fn highlight(self, inc: f32) -> [f32;4];
    fn darken(self, inc: f32) -> [f32;4];
}

impl ColorTricks for [f32;4]{
    fn highlight(self, inc: f32) -> [f32;4]{
        [
            (self[0] + inc).max(0.0).min(1.0),
            (self[1] + inc).max(0.0).min(1.0),
            (self[2] + inc).max(0.0).min(1.0),
            self[3],
        ]
    }
    fn darken(self, inc: f32) -> [f32;4]{
        [
            (self[0] - inc).max(0.0).min(1.0),
            (self[1] - inc).max(0.0).min(1.0),
            (self[2] - inc).max(0.0).min(1.0),
            self[3],
        ]
    }
}

/// The renderer of the interface
pub struct Renderer {
    device: Arc<Device>,

    depth_format: vk::Format,
    depth_image: Arc<Image>,

    color_format: vk::Format,
    color_image: Arc<Image>,

    render_pass: Arc<marp::framebuffer::RenderPass>,
    framebuffer: Arc<marp::framebuffer::Framebuffer>,

    render_area: Area,
    //Holds the currently used colors of the interface, can be changed at run time
    pub color_pallet: ColorPallet,

    dyn_descriptor_set: Arc<DescriptorSet>,
    
    //The following are all instances of the base primitives.
    icon: Icon,
    image: PrimImage,
    line: Line,
    rectangle: Rectangle,
    text: Text,

    next_calls: Vec<PrimitiveCall>,
}

impl Renderer {
    pub fn new(
        device: Arc<Device>,
        init_queue: Arc<Queue>,
        window_extent: (f64, f64),
        config: &Config,
        desired_format: vk::Format,
    ) -> Self {
        //init all main resources of the renderer. The pipelines are handled on a per-primitive basis.
        let depth_format = device
            .get_useable_format(
                vec![
                    vk::Format::D16_UNORM_S8_UINT,
                    vk::Format::D24_UNORM_S8_UINT,
                    vk::Format::D32_SFLOAT_S8_UINT,
                ],
                &ImageUsage {
                    depth_attachment: true,
                    input_attachment: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a depth format");

        let (width, height) = if window_extent.0 <= 0.0 || window_extent.1 <= 0.0 {
            panic!("The window extent should never become negative!");
        } else {
            (window_extent.0 as u32, window_extent.1 as u32)
        };

        let depth_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: width,
                height: height,
                samples: 1,
            },
            depth_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        let depth_image = marp::image::Image::new(
            device.clone(),
            depth_image_info,
            SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");
        // TODO transition to DepthStencilAttachment_read/write access mask, and DepthStencilOptimal Layout

        let color_format = device
            .get_useable_format(
                vec![desired_format],
                &ImageUsage {
                    color_attachment: true,
                    input_attachment: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a color format");

        let color_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: width,
                height: height,
                samples: 1,
            },
            color_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                color_attachment: true,
                input_attachment: true,
                transfer_src: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        let color_image = marp::image::Image::new(
            device.clone(),
            color_image_info,
            SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");
        // TODO transition to DepthStencilAttachment_read/write access mask, and ShaderReadOptimal Layout

        //Setup the renderpass we want to use while rendering the triangle
        let attachment_descs = vec![
            //The color attachment
            marp::framebuffer::AttachmentDescription::new(
                vk::SampleCountFlags::TYPE_1,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL, //After the renderpass, transition to TRANSFER_OPTIMAL
            ),
            //The depth image
            depth_image.to_attachment_description(
                vk::AttachmentLoadOp::CLEAR,
                vk::AttachmentStoreOp::STORE, //We store our depth to read from it later possibly
            ),
        ];

        //This are the subpasses this renderpass goes through. However, this is easy in a triangle example.
        let render_pass = marp::framebuffer::RenderPass::new(attachment_descs)
            .add_subpass(vec![
                marp::framebuffer::AttachmentRole::OutputColor,
                marp::framebuffer::AttachmentRole::DepthStencil,
            ])
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                vk::SUBPASS_EXTERNAL,                            //from external pass
                0,                                               //To first and only subpass
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //When color output has finished on src
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //Color output can start on this one
                vk::AccessFlags::empty(),                        //Don't know access type of source
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //We need color read and write support
            ))
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                0,
                vk::SUBPASS_EXTERNAL,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //Don't know access type of source
                vk::AccessFlags::empty(),
            ))
            .build(device.clone())
            .expect("Failed to build renderpass!");

        //Now build the first frame buffer
        let framebuffer = marp::framebuffer::Framebuffer::new(
            device.clone(),
            render_pass.clone(),
            vec![color_image.clone(), depth_image.clone()],
        )
        .expect("Failed to create first frame buffer");

        //Generate the static data that is bound to every descriptor set
        let color_pallet = ColorPallet{
            background_color: config.background_color,
            text_color: config.text_color,
            highlight_color: config.highlight_color,
            edge_color: config.edge_color,
            inc: config.increase,
        };

        //Create the semi dynamic descriptor set by creating the builder items and find the images and sampler

        let text_builder = TextBuilder::new(
            config,
            device.clone(),
            init_queue.clone(),
        );

        let icon_build = IconBuilder::new(
            config,
            device.clone(),
            init_queue.clone(),
        );

        let (text_img, text_sampler) = text_builder.get_image_and_sampler();
        let (icon_img, icon_sampler) = icon_build.get_image_and_sampler();
        
        let dyn_desc_set = create_static_descriptor_set(
            device.clone(),
            vec![
                //The text image
                DescResource::new_image(
                    0,
                    vec![(
                        text_img,
                        Some(text_sampler),
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    )],
                    vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                ),
                //The icon image we created
                DescResource::new_image(
                    1,
                    vec![(
                        icon_img,
                        Some(icon_sampler),
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    )],
                    vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                )
            ],
        ).expect("Failed to build image descriptor set!");

        let icon = icon_build.build(
            config,
            device.clone(),
            init_queue.clone(),
            render_pass.clone(),
            vec![*dyn_desc_set.layout()]
        );
        let line = Line::new(
            config,
            device.clone(),
            init_queue.clone(),
            render_pass.clone(),
        );
        let rectangle = Rectangle::new(
            config,
            device.clone(),
            init_queue.clone(),
            render_pass.clone(),
        );
        let text = text_builder.build(
            config,
            device.clone(),
            init_queue.clone(),
            render_pass.clone(),
            vec![*dyn_desc_set.layout()],
        );
        Renderer {
            device: device.clone(),

            depth_format,
            depth_image,

            color_format,
            color_image,

            render_pass: render_pass.clone(),
            framebuffer,

            render_area: Area {
                pos: (0.0, 0.0),
                extent: window_extent,
            },

            
            color_pallet,
            dyn_descriptor_set: dyn_desc_set.clone(),
            
            //The following are all instances of the base primitives.
            icon,
            image: PrimImage {},
            line,
            rectangle,

            text,
            next_calls: Vec::new(),
        }
    }

    ///Resizes the render targets to the new area.
    pub fn resize(&mut self, _queue: Arc<Queue>, new_area: Area) {
        let depth_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: new_area.extent.0 as u32,
                height: new_area.extent.1 as u32,
                samples: 1,
            },
            self.depth_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        self.depth_image = marp::image::Image::new(
            self.device.clone(),
            depth_image_info,
            SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");
        // TODO transition to DepthStencilAttachment_read/write access mask, and DepthStencilOptimal Layout

        let color_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: new_area.extent.0 as u32,
                height: new_area.extent.1 as u32,
                samples: 1,
            },
            self.color_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                color_attachment: true,
                input_attachment: true,
                transfer_src: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        self.color_image = marp::image::Image::new(
            self.device.clone(),
            color_image_info,
            SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");

        self.framebuffer = marp::framebuffer::Framebuffer::new(
            self.device.clone(),
            self.render_pass.clone(),
            vec![self.color_image.clone(), self.depth_image.clone()],
        )
        .expect("Failed to create first frame buffer");

        self.render_area = new_area;
    }

    ///Adds a Primitive rendering call to the next frame.
    pub fn add_primitive(&mut self, primitive: PrimitiveCall) {
        self.next_calls.push(primitive);
    }

    ///Adds several primitive calls to the next frame
    pub fn add_primitives(&mut self, mut primitives: Vec<PrimitiveCall>){
        self.next_calls.append(&mut primitives);
    }

    ///Clears all rendering calls on the renderer that are in queue
    pub fn clear_calls(&mut self){
        self.next_calls = Vec::new();
    }
    
    //Orders the primitives back to front and returns them
    fn get_rendering_primitives(&mut self) -> Vec<Vec<PrimitiveCall>>{
        let mut map: BTreeMap<u32, Vec<PrimitiveCall>> = BTreeMap::new();
        let mut primitives = Vec::new();
        //Drain the calls 
        primitives.append(&mut self.next_calls);

        for p in primitives{
            //Check if we got already some vec for this level
            if let Some(ref mut level_vec) = map.get_mut(&p.level){
                level_vec.push(p);
            }else{
                map.insert(p.level, vec![p]);
            }
        }

        //Now collect all primitives into one vec
        map.into_iter().map(|(_lvl, lvlvec)|{
            lvlvec
        }).collect()
    }

    ///Renders the currently added primitives to the color image.
    ///# Safety
    /// Assumes that the Command buffer is in recording state, but is not within a renderpass.
    pub fn render(&mut self, command_buffer: Arc<CommandBuffer>) {
        //Start the renderpass and collect all primitive calls.
        let clear_values = vec![
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [1.0, 1.0, 1.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let rendering_rect = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: vk::Extent2D {
                width: self.render_area.extent.0 as u32,
                height: self.render_area.extent.1 as u32,
            },
        };

        command_buffer
            .cmd_begin_render_pass(
                self.render_pass.clone(),
                self.framebuffer.clone(),
                rendering_rect,
                clear_values,
                vk::SubpassContents::INLINE,
            )
            .expect("Failed to start renderpass!");

        //bind a pipeline to satisfy the validation layers.
        //TODO remove when the bug is in the validation layers.
        command_buffer.cmd_bind_pipeline(
            vk::PipelineBindPoint::GRAPHICS, self.rectangle.get_pipeline().clone()
        ).expect("Failed to bind dummy pipeline while starting recording!");

        
        //Setup scissors and viewport
        command_buffer
            .cmd_set_viewport(vec![vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: self.render_area.extent.0 as f32,
                height: self.render_area.extent.1 as f32,
                min_depth: 0.0,
                max_depth: 1.0,
            }])
            .expect("Failed to set viewport");

        command_buffer
            .cmd_set_scissors(vec![rendering_rect])
            .expect("Failed to set scissors!");

        //Bind the texts DescSet to index 0
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::GRAPHICS,
                self.text.get_pipeline_layout(),
                0,
                vec![self.dyn_descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind descriptor set text pipeline");

        //Now lets generate the calls and go through them on a per level basis
        let ordered_calls = self.get_rendering_primitives();
        for level_calls in ordered_calls {
            //Now order the calls on a per_primitive_type basis
            let mut rects = Vec::new();
            let mut texts = Vec::new();
            let mut lines = Vec::new();
            let mut icons = Vec::new();
            
            for mut p in level_calls{
                //If the area of this call has one of its extends at zero... for instance something collapsed etc,
                //do not draw
                //Check if we have an area that is only outide of our render area
                p.area.clamp_within(&self.render_area);
                //Early return if unused
                if p.area.has_zero_dimension() {
                    continue;
                }
                
                match p.prim{
                    Primitive::Rect => rects.push(p),
                    Primitive::Text{content: _, size: _} => texts.push(p),
                    Primitive::Line{point_set: _, color: _, line_width: _} => lines.push(p),
                    Primitive::Icon(ref _ico) =>  icons.push(p),
                    _ => println!("Primitive not implemented!")
                }
            }
            
            //Now draw the primitives for this level
            //RECTANGLE
            if rects.len() > 0{
                //Do not set the scissors to the rectangle, since we often have "between" the pixels draws with this primitive.
                //Also the shader handled transform is good enough.
                
                command_buffer
                    .cmd_set_scissors(vec![rendering_rect])
                    .expect("Failed to set scissors!");
                
                self.rectangle.draw(command_buffer.clone(), rects, &self);
            }

            //TEXT
            if texts.len() > 0{
                self.text.draw(command_buffer.clone(), texts, &self);
            }

            //Line
            if lines.len() > 0{
                self.line.draw(command_buffer.clone(), lines, &self);
            }

            //Icons
            if icons.len() >0{
                self.icon.draw(command_buffer.clone(), icons, &self);
            }
            

            //TODO,
            //Image
            //Icon
        }
        //Now override the primitve calls to "Nothing"
        self.next_calls = Vec::new();

        command_buffer
            .cmd_end_render_pass()
            .expect("Failed to end renderpass!");
    }

    ///Returns the current color image. After rendering this image will be in `TRANSFER_SRC_OPTIMAL` layout.
    /// If `resize()` is called, this becomes invalid. As it won't be used to draw to any longer.
    pub fn get_current_color_image(&self) -> Arc<Image> {
        self.color_image.clone()
    }

    ///Returns the standard renderpass for this renderer.
    pub fn get_render_pass(&self) -> Arc<marp::framebuffer::RenderPass> {
        self.render_pass.clone()
    }

    ///Returns the current area used for rendering
    pub fn screen_area(&self) -> &Area {
        &self.render_area
    }

    pub fn text_primitive(&self) -> &Text {
        &self.text
    }

    pub fn pallet(&self) -> &ColorPallet{
        &self.color_pallet
    }
}
