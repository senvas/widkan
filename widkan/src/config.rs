/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};
use serde::Deserialize;
use serde::Serialize;
use std::fs::{File, OpenOptions};
use std::io::{Error, Write};

///The Config data
#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    ///The file this config was read from and will be saved to if wanted.
    pub path: String,

    ///Should point to a directory which contains all shaders in SPIR-V format. If "default", internal shaders are used.
    pub shader_directory: String,
    ///Should point to the image directory containing for instance the icon tile map. If "default" internal images and icon sets are used.
    pub image_directory: String,

    ///Base color of the background surface
    pub background_color: [f32;4],
    ///Base color for text related colors.
    pub text_color: [f32;4],
    ///Base color for highlighting
    pub highlight_color: [f32;4],
    ///Base color for interface edges of any kind,
    pub edge_color: [f32;4],
    ///The amount an color will be increased / decreased when its transformed to highlighted or darkened form.
    pub increase: f32,

    pub text_cursor_width: f32,

    ///The size of some normal text
    pub text_size: u32,
    ///The size of an Title, should be used rarely
    pub text_title_size: u32,
    ///The size of some heading.
    pub text_heading: u32,

    ///If not specified different, this will be the extent of a normal button.
    pub default_button_extent: (f64, f64),
    ///Defines the icon size on buttons.
    pub default_button_icon_size: f64,
    ///Defines the height of single line elements like a text_edit or and label.
    pub default_single_line_height: f64,
    
    ///Describes the how much percentage of the extent should be covered by one "scrol"
    pub scroll_speed: f64,
    ///Defines the width of the scroll bar in pixel.
    pub scroll_bar_width: u32,
    ///The border width of widgets in pixels
    pub border_width: u32,
    ///A slim none highlighted border, like around an TextEdit field
    pub slim_border_width: u32,
    
    ///The width at which a mouse press on some element will trigger resizing.
    pub resizing_trigger: u32,

    ///The path to the ttf font that should be used. If "default" the standard Roboto-Regular font will be used
    pub font_path: String,
    ///The quality of the fonts. The value means that the font will be sharp until characters of `font_quality`px size. Bigger characters might appear blurry.
    /// If the value is turned much higher up, expect worse performance. A lower quality will reduce performance impact but might look worse.
    pub font_quality: u32,
    ///A String that holds all displayable characters
    pub font_characters: String,

    ///Defines if the wayland support should be loaded. If true you won't be able to capture RenderDoc captures
    pub support_wayland: bool,
}

impl Default for Config {
    fn default() -> Self {
        Config{
            path: String::from("config.txt"),
            shader_directory: String::from("default"),
            image_directory: String::from("default"),

            background_color: [0.15, 0.15, 0.15, 1.0],
            text_color: [0.8, 0.8, 0.8, 1.0],
            highlight_color: [0.2, 0.4, 1.0, 1.0],
            edge_color: [0.25, 0.25, 1.0, 1.0],
            increase: 0.05,
            
            text_cursor_width: 2.0,

            text_size: 24,
            text_title_size: 40,
            text_heading: 32,

            default_button_extent: (100.0, 40.0),
            default_button_icon_size: 50.0,
            default_single_line_height: 40.0,
            
            scroll_speed: 0.1,
            scroll_bar_width: 10,
            border_width: 4,
            slim_border_width: 2,
            resizing_trigger: 20,
            font_path: String::from("default"),
            font_quality: 64,
            font_characters: "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890,;.:-_#'+*?!\\\"$%&/()=<>|{[]}¸`öÖäÄüÜ′¹²³¼½¬".to_string(),
            support_wayland: false,
        }
    }
}

impl Config {
    //Saves the Config to some path
    pub fn save(&self) -> Result<(), Error> {
        let pretty = PrettyConfig {
            depth_limit: 2,
            separate_tuple_members: true,
            enumerate_arrays: true,
            ..PrettyConfig::default()
        };

        let final_string = to_string_pretty(self, pretty).expect("Failed to encode config!");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(self.path.clone())?;
        write!(file, "{}", final_string)?;
        Ok(())
    }

    //Loads a config from some file
    pub fn load(path: &str) -> Self {
        let file = match File::open(path) {
            Ok(f) => f,
            Err(e) => {
                println!(
                    "Could not find file at {} with er: {}, loading defaults!",
                    path, e
                );
                let mut default = Config::default();
                default.path = String::from(path);
                return default;
            }
        };
        let config: Config = match from_reader(file) {
            Ok(c) => c,
            Err(e) => {
                println!(
                    "Failed to parse config at {} with err: {}, loading defaults!",
                    path, e
                );
                Config::default()
            }
        };
        config
    }
}
